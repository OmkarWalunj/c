//dangling pointer in malloc


#include <stdio.h>
#include<stdlib.h>

int * ptr2=NULL;

void dangling(int x){

	int * ptr1=(int*)malloc(sizeof(int));

	*ptr1=x;

	printf("%d\n",*ptr1);  //10

	ptr2=ptr1;

	printf("%d\n",*ptr2);  //10

	free(ptr1);
	printf("%d\n",*ptr2);  //Garbage value
}

void main(){

	dangling(10);
}
