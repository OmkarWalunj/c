// WAP to show the dangling pointer in malloc


#include <stdio.h>
#include<stdlib.h>
int * ptr2 =NULL;

void fun(){

	int * ptr1=(int*)malloc(sizeof(int));

	*ptr1=10;
	
	printf("%d\n",*ptr1); //10
	printf("%p\n",ptr2);  //(nil)

	ptr2=ptr1;

	printf("%d\n",*ptr1); //10
        printf("%d\n",*ptr2);  //10
	
	free(ptr1);
	printf("%d\n",*ptr1);  //0 or Garbage 
        printf("%d\n",*ptr2);  //0 or Garbage
}

void main(){

	fun();
}
