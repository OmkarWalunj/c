//WAP to allocate memory of float type take value from user and use malloc function

#include <stdio.h>
#include <stdlib.h>

void main(){

	float *ptr=(float *)malloc(sizeof(float));

	*ptr=20.256;

	printf("%f\n",*ptr);

	free(ptr);
}
