//register variable  and auto variable

#include <stdio.h>

//auto int y=10;       //limitations of scope in global variable
register int z=20;  //error
void main(){

	register int x=10;

	//printf("%d\n",&x);   //address of register variable ‘x’ requested

}
