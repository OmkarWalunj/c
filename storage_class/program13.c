//  parameter storage class

#include <stdio.h>

void fun(auto int x, auto int y){   //error storage class specified for parameter
	int a;
	register int b=20;
	static int c=40;
	extern int d;
}
void main(){
	fun(20,30);
}
