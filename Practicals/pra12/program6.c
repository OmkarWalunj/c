/*D	C	B	A	
 	e	f	g	
 	 	F	E	
 	 	 	g	
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter number of rows: ");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=rows+i-1;
		for (int j=1;j<=rows;j++){
			if(i%2==0){
				if (j<i){
					printf(" \t");
				}else{
					printf("%c\t",96+num);
					num++;
				}
			}else{
				if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%c\t",num+64);
                                        num--;
				}
			}
		}
		printf("\n");
	}
}

