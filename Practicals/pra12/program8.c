/*		1
 *	    1   b   3
 *	1   b   3   d   5
 *   1  b   3   d   5   e   7
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=1;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			if(num %2==0){
				printf("%c\t",num+96);
				num++;
			}else{
				printf("%d\t",num);
                                num++;
			}

		}
		printf("\n");
	}
}
