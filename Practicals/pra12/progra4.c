/*	5	D	3	B	1	
	D	3	B	1	
	3	B	1	
	B	1
*/
#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows :\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=rows-i+1;
		for (int j=1;j<=rows-i+1;j++){
			if(num%2==0){
				printf("%c\t",num+64);
			}else{
				printf("%d\t",num);
			}
			num--;
		}
		printf("\n");
	}
}
