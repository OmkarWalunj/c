/*		1
 *	    4   2   4
 *	9   6   3   6   9
 *	    4   2   4
 *	        1
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols,num;

	for (int i=1;i<2*rows;i++){
		int num=1;
		if(i<=rows){
			num=i*i;
			sp=rows-i;
			cols=2*i-1;
		}else{
			sp=i-rows;
			cols=cols-2;
			num=(2*rows-i)*(2*rows-i);
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			if(i<=rows){
				if (j<i){
					printf("%d\t",num);
					num=num-i;
				}else{
					printf("%d\t",num);
					num=num+i;
				}
			}else{
				if(j<2*rows-i){
					printf("%d\t",num);
					num=num-(2*rows-i);
				}else{
					printf("%d\t",num);
                                        num=num+(2*rows-i);
				}
			}
		}
		printf("\n");
	}
}
