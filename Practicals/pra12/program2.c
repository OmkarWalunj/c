/*			1
 *	             1  b
 *	          1  b  2
 *	       1  b  2  d
 *	    1  b  2  d  3
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Entr rows:\n");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=1;
		char ch='a';

		for(int space=1;space <=rows-i;space++){
			printf(" \t");
		}

		for(int j=1;j<=i;j++){
			if (j%2==0){
				printf("%c\t",ch);
			}else{
				printf("%d\t",num);
				num++;
			}
			ch++;
		}
		printf("\n");
	}
}

