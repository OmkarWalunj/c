/*	1   4   7   10
 *	7   10  13  16
 *      13  16  19  22
 *      19  22  25  28
 */

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows :\n");
	scanf("%d",&rows);
	int num1=1;
	int num2=2*rows-1;

	for(int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			if(i%2==0){
				printf("%d\t",num2);
				num2=num2+3;
			}else{
			        printf("%d\t",num1);
                                num1=num1+3;
			}
		}
		printf("\n");
	}
}


