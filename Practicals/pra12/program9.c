/*  g	6	e	4	c	2	a	
 	5	d	3	b	1	
 	 	c	2	a	
 	 	 	1	
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num=1;

	for(int i=1;i<=rows;i++){
		int num=2*rows-2*i+1;
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			if(i%2 !=0){
				if(k%2==0){
					printf("%d\t",num);
				}else{
					printf("%c\t",num+96);
				}
				num--;
			}else{
				 if(k%2==0){
                                        printf("%c\t",num+96);
                                 }else{
                                        printf("%d\t",num);
                                 }
  			         num--;
			}

		}
		printf("\n");
	}
}
