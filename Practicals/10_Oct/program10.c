/*   1  3  5
 *   7  9  11
 *   13 15 17
 */

#include <stdio.h>

void main(){

	int rows , num=1;

	printf("Enter rows:");
	scanf("%d",&rows);
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
		       printf("%d\t",num);
		       num += 2;
		}
		printf("\n");
	}
}

