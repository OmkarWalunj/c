/*   1  1  1
 *   2  2  2
 *   3  3  3  
 */

#include <stdio.h>

void main(){
	int rows,num=1;

	printf("Enter rows:");
	scanf("%d",&rows);

	for (int i =0;i<rows;i++){
		for (int j=0;j<rows;j++){
			printf("%d\t",num);
		}
		num++;
		printf("\n");
	}
}
