/*   A   A   A
 *   B   B   B
 *   C   C   C
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch ='A';
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
		       printf("%c\t",ch);
		}
		ch++;
		printf("\n");
	}
}

