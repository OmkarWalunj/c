/*    1  2  3  4 
 *    5  6  7  8
 *    9 10 11 12
 */

#include <stdio.h>

void main(){

	int rows,cols,num=1;

	printf("Enter rows:");
	scanf("%d",&rows);
	
	printf("Enter cols:");
	scanf("%d",&cols);

	for (int i=0;i<rows;i++){
		for (int j=0;j<cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}
