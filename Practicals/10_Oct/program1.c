/* WAP to print the square of odd number between a given range from user 
 * input : Enter start : 10
 *         Enter End   : 20
 * output : 121  169  225  361
 */

#include <stdio.h>

void main(){

	int start ,end;
	printf("Enter Start:");
	scanf("%d",&start);
	
	printf("Enter end :");
	scanf("%d",&end);

	for (int i= start ; i<=end;i++){
		if (i % 2 != 0){
			printf("%d  " ,i*i);
		}
	}
	printf("\n");
}
