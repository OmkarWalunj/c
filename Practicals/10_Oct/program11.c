/*   1  2  3  4
 *   a  b  c  d
 *   5  6  7  8
 *   e  f  g  h
 */

#include <stdio.h>

void main(){

	int rows , num=1;
	char ch='a';

	printf("Enter rows:");
	scanf("%d",&rows);
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			if ( i % 2 ==0){
		        	printf("%d\t",num);
				num++;
			}else{
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}

