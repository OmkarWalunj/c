/* WAP to print the number are divisible by 3 & 7 in reverse order  between a given range from user 
 * input : Enter start : 20
 *         Enter End   : 80
 * output : 63  42 21 
 */

#include <stdio.h>

void main(){

	int start ,end;
	printf("Enter Start:");
	scanf("%d",&start);
	
	printf("Enter end :");
	scanf("%d",&end);

	for (int i= end ; i>=start;i--){
		if (i % 3 == 0 && i%7 == 0){
			printf("%d  " ,i);
		}
	}
	printf("\n");
}
