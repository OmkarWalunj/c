/*    a  b  c  
      d  e  f
      g  h  i   */

#include <stdio.h>

void main(){
	int rows;
	char ch='a';

	printf("Enter rows:");
	scanf("%d",&rows);

	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			printf("%c\t",ch);
			ch++;
		}
		printf("\n");
	}
}
