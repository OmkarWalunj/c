/*   1   2    3  
 *   2   3    4
 *   3   4    5
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	for (int i=0;i<rows;i++){
		int num=i+1;
		for (int j=0;j<rows;j++){
			printf("%d\t",num);
			num ++;
		}
		printf("\n");
	}
}
