/*WAP to find whether the given input character is an alphabet , a digit ,or a special character */

#include <stdio.h>

void main(){

	char ch ;
	printf ("Enter Character :");
	scanf("%c",&ch);

	if ((ch >= 'A' && ch<='Z') || ( ch>='a' && ch<='z')){
		printf("%c is a alphabet \n",ch);
	}else if(ch >= '0' && ch<='9'){
		printf("%c is a digit \n",ch);
	}else {
		printf("%c is a special character\n",ch);
	}
}
