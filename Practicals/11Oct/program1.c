/*WAP to print the odd number as it and cube even number beetween a given range*/
//Output : 1000  11  1728  13  2744 15

#include <stdio.h>

void main(){

	int start,end;

	printf("Enter starting number:\n");
	scanf("%d",&start);

	printf("Enter ending number:\n");
	scanf("%d",&end);

	for (int i=start; i<=end;i++){
		if ( i% 2!=0){
			printf("%d  ",i);
		}else{
			printf("%d  ",i*i*i);
		}
	}
	printf("\n");
}

