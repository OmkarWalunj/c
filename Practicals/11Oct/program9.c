/*   A  b  C
 *   d  E  f
 *   G  h  I
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int  ch = 65;
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			if (ch % 2 != 0){
				printf("%c\t",ch);
			}else {
				printf("%c\t",ch+32);
			}
			ch++;
		
		}
		printf("\n");
	}
}
