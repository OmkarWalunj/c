/*   D  D  D  D
 *   C  C  C  C
 *   B  B  B  B
 *   A  A  A  A
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch = 64+rows;
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			printf("%c\t",ch);
		
		}
		ch--;
		printf("\n");
	}
}
