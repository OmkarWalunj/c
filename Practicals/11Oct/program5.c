/*   a  B  c
 *   d  E  f
 *   g  H  i
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch = 'a';
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			if (j% 2 != 0){
				printf("%c\t",ch-32);
			}else {
				printf("%c\t",ch);
			}
			ch++;
		
		}
		printf("\n");
	}
}
