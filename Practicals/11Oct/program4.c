/*   d  d  d  d
 *   c  c  c  c
 *   b  b  b  b
 *   a  a  a  a
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch = 96+rows;
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			printf("%c\t",ch);
		
		}
		ch--;
		printf("\n");
	}
}
