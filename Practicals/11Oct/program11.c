/*   1  2  3  4
 *   a  b  c  d
 *   #  #  #  #
 *   e  f  g  h
 */

#include <stdio.h>

void main(){

	int rows,num=1;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch = 'a';
	for (int i=0;i<rows;i++){
		for (int j=0;j<4;j++){
			if (i%3==1){
				printf("%c\t",ch);
				ch++;
			}else if(i%3==0){
				printf("%d\t",num);
				num++;
			}else {
				printf("#\t");
			}
		}
		printf("\n");
	}
}
