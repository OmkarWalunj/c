/*   1   4    7  
 *   10  13  16
 *   19  22  25
 */

#include <stdio.h>

void main(){

	int rows,num=1;

	printf("Enter rows:");
	scanf("%d",&rows);
	for (int i=0;i<rows;i++){
		for (int j=0;j<rows;j++){
			printf("%d\t",num);
			num += 3;
		}
		printf("\n");
	}
}
