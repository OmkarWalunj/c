//matrix transpose
//
//-you are given a 2D integer matrix A and return another matrix which is transpose of A


#include <stdio.h>

int cols=-1;

void transpose(int arr[][cols],int arr1[][cols],int rows,int cols){
	for (int i=0;i<cols;i++){
		for (int j=0;j<rows;j++){
			arr1[i][j]=arr[j][i];
		}
	}
}

void main(){

	int rows;

	printf("Enter no of rows 2D array:\n");
	scanf("%d",&rows);

	printf("Enter no of cols 2D array:\n");
	scanf("%d",&cols);

	int arr[rows][cols];

	printf("Enter array elements:\n");

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	int arr1[cols][rows];

	transpose(arr,arr1,rows,cols);

	 for (int i=0;i<cols;i++){
                for (int j=0;j<rows;j++){
			printf("%d ",arr1[i][j]);

                }
		printf("\n");
        }


}


