//column sum
//
//-you are given a 2D integer matrix A and return a !D integer array containing
//column-wise sums of the original matrix
//-Return an array containing column-wise sums of the original matrix

#include <stdio.h>

int cols=-1;

int * sumcols(int arr[][cols],int * arr1,int x,int y){
	int sum =0;
	for (int i=0;i<y;i++){
		sum=0;
		for (int j=0;j<x;j++){
			sum =sum+*(*(arr+j)+i);
		}
		arr1[i]=sum;
	}
	return arr1;
}

void main(){

	int rows;

	printf("Enter no of rows 2D array:\n");
	scanf("%d",&rows);

	printf("Enter no of cols 2D array:\n");
	scanf("%d",&cols);

	int arr[rows][cols];

	printf("Enter array elements:\n");

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	int arr1[cols];
	int *ptr=sumcols(arr,arr1,rows,cols);

	for(int i=0;i<cols;i++){
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}


