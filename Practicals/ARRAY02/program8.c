//are matrices the same
//
//-you are given a 2D integer matrix A and return if same  1 and not 0


#include <stdio.h>


int same(int arr[][3],int arr1[][3],int rows,int cols){
	for (int i=0;i<cols;i++){
		for (int j=0;j<rows;j++){
			if(arr1[i][j] !=arr[i][j]){
				return 0;
			}
		}
	}
	return 1;
}

void main(){

	int arr[3][3]={1,2,3,4,5,6,7,8,9};
	
	int arr1[3][3]={1,2,3,4,5,6,7,8,9};

	printf("%d\n",same(arr,arr1,3,3));


}


