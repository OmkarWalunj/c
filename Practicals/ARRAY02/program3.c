//main diagonal sum
//
//-you are given a 2D integer matrix A and return a sum integer  containing
// sums of the main daigonal elements of original matrix


#include <stdio.h>

int cols=-1;

int  sumdia(int arr[][cols],int x,int y){
	int sum =0;
	for (int i=0;i<x;i++){
		for (int j=0;j<y;j++){
			if(i==j){
				sum =sum+*(*(arr+i)+j);
			}
		}
	}
	return sum;
}

void main(){

	int rows;

	printf("Enter no of rows 2D array:\n");
	scanf("%d",&rows);

	printf("Enter no of cols 2D array:\n");
	scanf("%d",&cols);

	int arr[rows][cols];

	printf("Enter array elements:\n");

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			scanf("%d",&arr[i][j]);
		}
	}

	int val=sumdia(arr,rows,cols);
	printf("Output :%d\n",val);

}


