//Rotate matrix
//
//-you are given a 2D integer matrix A 
// rotate the image by 90 degrees


#include <stdio.h>

int N=-1;

void transpose(int arr[][N],int arr1[][N],int N){
	int k=0;
	for (int i=N-1;i>=0;--i){
		for (int j=0;j<N;j++){
			arr1[j][k]=arr[i][j];
		}
		k++;
	}
}

void main(){

	printf("Enter N 2D array:\n");
	scanf("%d",&N);

	int arr[N][N];

	printf("Enter array elements:\n");

	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	int arr1[N][N];

	transpose(arr,arr1,N);

	 for (int i=0;i<N;i++){
                for (int j=0;j<N;j++){
			printf("%d ",arr1[i][j]);

                }
		printf("\n");
        }


}


