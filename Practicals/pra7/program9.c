/*		1
 *	    4  	7   4
 *	7   10  13  10   7
 *   10 13  16  19  16   13   10
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);
	int num=1;
	for (int i=1;i<=rows;i++){
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		int num1=num;
		for (int k=1;k<=2*i-1;k++){
			if (k<i){
				printf("%d\t",num1);
				num1=num1+3;
			}else{
				printf("%d\t",num1);
				num1=num1-3;
			}
		}
		num=num+3;
		printf("\n");
	}
}
