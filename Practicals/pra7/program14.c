/*		4
 *	    3  	6   3
 *	2   4   6   4   2
 *   1  2   3   4   3   2   1
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);
	int num=rows;
	for (int i=1;i<=rows;i++){
		int num1=num;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			if (k<i){
				printf("%d\t",num1);
				num1=num1+num;
			}else{
				printf("%d\t",num1);
				num1=num1-num;
			}
		}
		num--;
		printf("\n");
	}
}
