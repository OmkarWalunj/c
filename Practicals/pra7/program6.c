/*		4
 *	    3  	3   3
 *	2   2   2   2   2
 *   1  1   1   1   1   1   1
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=rows-i+1;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			printf("%d\t",num);
		}
		printf("\n");
	}
}
