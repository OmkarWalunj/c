/*		d
 *	    c  	c   c
 *	b   b   b   b   b
 *   a  a   a   a   a   a   a
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=96+rows-i+1;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			printf("%c\t",num);
		}
		printf("\n");
	}
}
