/*		A
 *	    b  	a   b
 *	C   E   G   E   C
 *   d  c   b   a   b   c   d
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=64+i;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			if(i%2==0){
				if (k<i){
					printf("%c\t",num+32);
					num--;
				}else{
					printf("%c\t",num+32);
					num++;
				}
			}else{
				 if (k<i){
                                        printf("%c\t",num);
                                        num=num+2;
                                }else{
                                        printf("%c\t",num);
                                        num=num-2;
                                }

			}

		}
		printf("\n");
	}
}
