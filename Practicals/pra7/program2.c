/*		1
 *	    1  	2   3
 *	1   2   3   4   5
 *   1  2   3   4   5   6   7
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=1;
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}
