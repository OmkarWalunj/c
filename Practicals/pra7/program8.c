/*		d
 *	    C  	C   C
 *	b   b   b   b   b
 *   A  A   A   A   A   A   A
 */

#include<stdio.h>

void main(){

	int rows;
	printf ("Enter no of rows:");
	scanf("%d",&rows);
	int num=64+rows;
	for (int i=1;i<=rows;i++){
		for (int sp=i;sp<rows;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*i-1;k++){
			if (i%2==0){
				printf("%c\t",num);
			}else{
				printf("%c\t",num+32);
			}
		}
		num--;
		printf("\n");
	}
}
