/*	1   2   3   4
 *	4   5   6
 *	6   7
 *	7
 */

#include <stdio.h>

void main(){
	int rows,num=1;

	printf("Enter rows:");
	scanf("%d",&rows);
	for (int i=1 ; i<=rows;i++){
		for (int j=rows;j>=i;j--){
			printf("%d\t",num);
			num++;
		}
		num--;
		printf("\n");
	}
}
