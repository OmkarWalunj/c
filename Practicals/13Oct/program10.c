/*	4  3   2   1
 *	C  B  A  
 *	2  1
 *	A
 */

#include <stdio.h>

void main(){
	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);

	for (int i=1 ; i<=rows;i++){
		int num =rows-i+1;
		for (int j=rows;j>=i;j--){
			if (i%2==0){
				printf("%d\t",num);
			}else{
				printf("%c\t",num+64);
			}
			num--;
		}
		printf("\n");
	}
}
