/*	d  d   d  d
 *	C  C   C  
 *	b  b
 *	A
 */

#include <stdio.h>

void main(){
	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);

	for (int i=0 ; i<rows;i++){
		int num =96+rows-i;
		for (int j=rows;j>i;j--){
			if (i%2==0){
				printf("%c\t",num);
			}else{
				printf("%c\t",num-32);
			}
		}
		printf("\n");
	}
}
