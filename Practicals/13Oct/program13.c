/*WAP to print the factorial of given input range from user 
 * input  :   start = 1
 * 	      End   =5
 * output :   Factorial of 1 is 1 
 * 	      Factorial of 2 is 2
	      Factorial of 3 is 6 
	      Factorial of 4 is 24 
	      Factorial of 5 is 120
 */

#include <stdio.h>

void main(){

	int start,End;
	printf("Enter start number:");
	scanf("%d",&start);
	printf("Enter end number:");
        scanf("%d",&End);
        
 	for (int i=start;i<=End;i++){	
		int temp=i;
		long store =1;
		while (temp>=1){
			store =store * temp;
			temp--;	
		}
		printf("Factorial of %d is %ld \n",i,store);
	}
}
