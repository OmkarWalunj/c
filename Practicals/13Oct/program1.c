/*WAP to print the factorial of given input number from user 
 * input  :   5
 * output :   Factorial of 5 is 120 
 */

#include <stdio.h>

void main(){

	int num;
	printf("Enter number:");
	scanf("%d",&num);
	int temp =num;
	long int store=1;               
//	printf("%ld\n",sizeof(store));               //8
	while (temp>=1){
		store =store * temp;
		temp--;	
	}
	printf("Factorial of %d is %ld \n",num,store);
}
