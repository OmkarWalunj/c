/*	a   b   c   d
 *	a   b   c
 *	a   b
 *	a
 */

#include <stdio.h>

void main(){
	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);

	for (int i=1 ; i<=rows;i++){
		int num =97;
		for (int j=rows;j>=i;j--){
			printf("%c\t",num);
			num++;
		}
		printf("\n");
	}
}
