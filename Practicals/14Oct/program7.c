/*		1
 *          A   b
 *      1   2   3
 *  a   b   c   d
 
 */

#include <stdio.h>

void main(){

	int rows,num;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	
	
	for (int i=1;i<=rows;i++){
		num=1;
		char ch='A';
		for (int j=1;j<=rows;j++){
			if (i%2!=0){
				if (j<=rows-i){
					printf(" \t");
				}else{
					printf("%d\t",num);
					num++;
				}
			}else{
				if(j%2 == 0){
					if (j<=rows-i){
                                        	printf(" \t");
                                	}else{
                                        	printf("%c\t",ch+32);
                                        	ch++;
					}
                                }else{ if (j<=rows-i){
                                                printf(" \t");
                                        }else{
                                                printf("%c\t",ch);
                                                ch++;
					}
				}

			}
	
		}
		printf("\n");
	}
}


