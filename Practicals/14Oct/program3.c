/*		d
 *          c   c
 *      b   b   b
 *  a   a   a   a
 */

#include <stdio.h>

void main(){

	int rows;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	int num=96+rows;
	for (int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			if (j<=rows-i){
				printf(" \t");
			}else{
				printf("%c\t",num);
				
			}
		}
		num--;
		printf("\n");
	}
}


