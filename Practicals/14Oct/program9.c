/*		4
 *          3   6
 	2   4   6
     1  2   3   4
 */

#include <stdio.h>

void main(){

	int rows;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		int num=rows-i+1;
		for (int j=1;j<=rows;j++){
			if (j<=rows-i){
				printf(" \t");
			}else{
				printf("%d\t",num);
				num=num+rows-i+1;
			}
		}
		printf("\n");
	}
}


