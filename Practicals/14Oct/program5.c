/*		D
 *          c   D
 *      B   c   D
 *  a   B   c   D
 */

#include <stdio.h>

void main(){

	int rows,num;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	
	
	for (int i=1;i<=rows;i++){
		num=65+rows-i;
		for (int j=1;j<=rows;j++){
			if (j%2==0){
				if (j<=rows-i){
					printf(" \t");
				}else{
					printf("%c\t",num);
					num++;
				}
			}else{
				if (j<=rows-i){
                                        printf(" \t");
                                }else{
                                        printf("%c\t",num+32);
                                        num++;
                                }
			}
	
		}
		printf("\n");
	}
}


