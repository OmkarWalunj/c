/*11.WAP to print the addition of factorial of two given number from user
 * input num1=4
 * 	 num2=5
 * output: Addition of factorial of 4 and 5 is 144
 */

#include <stdio.h>

void main(){
	int num1,num2;

	printf("Enter number: ");
	scanf("%d",&num1);
	printf("Enter number: ");
        scanf("%d",&num2);
        int  temp1=num1;
	int temp2=num2;

	int fact1=1;
	int fact2=1;
	while ( temp1 >=1){

		fact1 = fact1* temp1;
		temp1--;
	}
	printf("%d\n",fact1);
	while ( temp2 >=1){

                fact2 = fact2* temp2;
                temp2--;
        }
	printf("%d\n",fact2);
	printf("Addition of factorial of %d and %d is %d\n",num1,num2,fact1+fact2);
}
