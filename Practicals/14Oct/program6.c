/*		5
 *          5   6
 *      5   4   3
 *  5   6   7   8
 5  4   3   2   1
 */

#include <stdio.h>

void main(){

	int rows,num;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	
	
	for (int i=1;i<=rows;i++){
		num=rows;
		for (int j=1;j<=rows;j++){
			if (i%2==0){
				if (j<=rows-i){
					printf(" \t");
				}else{
					printf("%d\t",num);
					num++;
				}
			}else{
				if (j<=rows-i){
                                        printf(" \t");
                                }else{
                                        printf("%d\t",num);
                                        num--;
                                }
			}
	
		}
		printf("\n");
	}
}


