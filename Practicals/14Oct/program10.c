/*	              1
 *            4       9
 *      64    125    216
  2401  4096  6561  10000
 */

#include <stdio.h>

void main(){

	int rows,num=1;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			int num1=i;
			if (j<=rows-i){
				printf(" \t");
			}else{
				int value=1,base=num;
				while(num1 != 0){
					value *= base;
					num1--;
				}
				printf("%d\t",value);
				num++;
			}
		}
		printf("\n");
	}
}


