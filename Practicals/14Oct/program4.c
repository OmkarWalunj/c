/*		1
 *          4   7
 *      10  13  16
 */

#include <stdio.h>

void main(){

	int rows,num=1;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			if (j<=rows-i){
				printf(" \t");
			}else{
				printf("%d\t",num);
				num=num+rows;
			}
		}
		printf("\n");
	}
}


