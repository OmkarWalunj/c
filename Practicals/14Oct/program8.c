/*		A
 *          b   a
 *      C   E   G
 *  d   c   b   a
 E  G   I   K   M
 */

#include <stdio.h>

void main(){

	int rows,num;
 	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	
	
	for (int i=1;i<=rows;i++){
		num=64+i;
		for (int j=1;j<=rows;j++){
			if (i%2!=0){
				if (j<=rows-i){
					printf(" \t");
				}else{
					printf("%c\t",num);
					num++;
				}
			}else{
				if (j<=rows-i){
                                        printf(" \t");
                                }else{
                                        printf("%c\t",num+32);
                                        num--;
                                }
			}
	
		}
		printf("\n");
	}
}


