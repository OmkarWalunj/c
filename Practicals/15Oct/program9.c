/*	100  9  64  7
 *	     36  5  16
 *	         3  4
 *	            1
 */

#include<stdio.h>

void main(){

	int rows,num;
	printf("Enter number of rows: ");
	scanf("%d",&rows);
	num= rows*(rows+1)/2;
	for (int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			if (num %2 ==0){
				if (j<i){
					printf(" \t");
				}else{
					printf("%d\t",num*num);
					num--;
				}
			}else{
				if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%d\t",num);
                                        num--;
                                }
			}

		}
		printf("\n");
	}
}

