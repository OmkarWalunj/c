/*	1  2  3  4
 *	   2  3  4
 *	      3  4
 *	         4
 */

#include<stdio.h>

void main(){

	int rows,num=1;
	printf("Enter number of rows: ");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		num=rows+1-i;
		for (int j=1;j<=rows;j++){
			if (j<i){
				printf(" \t");
			}else{
				printf("%d\t",num);
				num--;
			}
		}
		printf("\n");
	}
}

