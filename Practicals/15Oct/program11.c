/*	1   3   5   7
 *	    7   5   3
 *	        3   5
 *	            5
 */

#include<stdio.h>

void main(){

	int rows,num1,num2;
	printf("Enter number of rows: ");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		num1=i;
		for (int j=1;j<=rows;j++){
			 if (i %2 ==0){
                                if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%d\t",num2);
                                        num2=num2-2;
                                }
                        }
			 else{
                                if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%d\t",num1);
                                        num1=num1+2;
                                }
			}

		}
		num2=num1-2;
		printf("\n");
	}
}

