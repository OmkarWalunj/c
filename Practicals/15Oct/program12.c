/*	A   b   C   d
 *	    e   G   i
 *	        K   n
 *	            q
 */

#include<stdio.h>

void main(){

	int rows,num=97;
	printf("Enter number of rows: ");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){

		for (int j=1;j<=rows;j++){
			if(j%2==0){
				if (j<i){
					printf(" \t");
				}else{
					printf("%c\t",num);
					num=num+i;
				}
			}else{
				if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%c\t",num-32);
                                        num=num+i;
				}
			}
		}
		printf("\n");
	}
}

