/*	d   d   d   d
 *	    c   c   c
 *	        b   b
 *	            a
 */

#include<stdio.h>

void main(){

	int rows,num=1;
	printf("Enter number of rows: ");
	scanf("%d",&rows);

	for (int i=1;i<=rows;i++){
		num=97+rows-i;
		for (int j=1;j<=rows;j++){
			if (j<i){
				printf(" \t");
			}else{
				printf("%c\t",num);
				//num++;
			}
		}
		printf("\n");
	}
}

