/*	 a   B   c  D
 *	     e   F  g
 *	         H  i
 *	            J
 */

#include<stdio.h>

void main(){

	int rows,num;
	printf("Enter number of rows: ");
	scanf("%d",&rows);
	num=97;
	for (int i=1;i<=rows;i++){
		for (int j=1;j<=rows;j++){
			if (num %2 ==0){
				if (j<i){
					printf(" \t");
				}else{
					printf("%c\t",num-32);
					num++;
				}
			}else{
				if (j<i){
                                        printf(" \t");
                                }else{
                                        printf("%c\t",num);
                                        num++;
                                }
			}

		}
		printf("\n");
	}
}

