/*   d  d  d  d  d  d  d  
 *      c  c  c  c  c
 *         b  b  b
 *            a
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num=64+rows;
	for(int i=1;i<=rows;i++){
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			printf("%c\t",num);
		}
		num--;
		printf("\n");
	}
}
