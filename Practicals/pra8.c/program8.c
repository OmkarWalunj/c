/*   D  C  B  A  B  C  D
 *      c  b  a  b  c
 *         B  A  B
 *            a
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=64+rows-i+1;
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			if (i%2 !=0){
				if(k<=rows-i){
					printf("%c\t",num);
					num--;
				}else{
					printf("%c\t",num);
					num++;
				}
			}else{
				  if(k<=rows-i){
                                        printf("%c\t",num+32);
                                        num--;
                                }else{
                                        printf("%c\t",num+32);
                                        num++;
                                }

			}
		}
		printf("\n");
	}
}
