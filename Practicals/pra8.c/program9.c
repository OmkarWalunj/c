/*   A0  B1  C2  D3  E4  F5  G6
 *       H2  I3  J4  K5  L6
 *           M4  N5  O6
 *               P6
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num=0;
	int num1=65;
	for(int i=1;i<=rows;i++){
		int num2=num;
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			printf("%c%d\t",num1,num2);
			num1++;
			num2++;
		}
		num=num+2;
		printf("\n");
	}
}
