/*   1  3  5  7  9  7  5  3  1
 *      9  7  5  3  5  7  9
 *         3  5  7  5  3
 *            7  5  7
 *               5
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num=1;

	for(int i=1;i<=rows;i++){
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		int num1=num;
		for (int k=1;k<=2*rows-2*i+1;k++){
			if(k==rows-i+1){
				num=num1;
			}
			if(i%2==0){
				if(k<=rows-i){
					printf("%d\t",num1);
					num1=num1-2;
				}else{
					printf("%d\t",num1);
					num1=num1+2;
				}
			}else{
				  if(k<=rows-i){
                                        printf("%d\t",num1);
                                        num1=num1+2;
                                }else{
                                        printf("%d\t",num1);
                                        num1=num1-2;
                                }

			}
		}
		printf("\n");
	}
}
