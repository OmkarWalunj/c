/*   7  6  5  4  3  2  1
 *      5  4  3  2  1
 *         3  2  1
 *            1
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num=1;

	for(int i=1;i<=rows;i++){
		int num=2*rows-2*i+1;
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			printf("%d\t",num);
			num--;
		}
		printf("\n");
	}
}
