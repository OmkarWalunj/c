/*   *  *  *  #  *  *  *
 *      *  *  #  *  *
 *         *  #  *
 *            #
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int sp=1;sp<i;sp++){
			printf(" \t");
		}
		for (int k=1;k<=2*rows-2*i+1;k++){
			if(k==rows-i+1){
				printf("#\t");
			}else{
				printf("*\t");
			}

		}
		printf("\n");
	}
}
