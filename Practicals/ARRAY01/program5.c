//rotation array
//
//problem Description
//-GIven an array A and an N integer
//-

#include<stdio.h>

int* rotation( int * arr,int size,int Rot){
	int a;
	int b;
	while(Rot){
		a=arr[size-1];
		for(int i=0;i<size;i++){
			b=arr[i];
			arr[i]=a;
			a=b;
		}
		Rot--;
	}
	return arr;
}
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter array Elements :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int rota;
	printf("ENter No of rotation:\n");
	scanf("%d",&rota);

	int * ptr=rotation(arr,size,rota);

	printf("Rotation array :\n");

	for(int i=0 ;i<size;i++){
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}
