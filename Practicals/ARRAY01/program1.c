// count of element
//  --count the number of elements that have at least 1 element greater than itself

#include<stdio.h>

int count(int *arr,int size){
	
	int count=0;
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			if(arr[i]<arr[j]){
				count++;
				j=size;
			}
		}
	}
	return count;
}

void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter array Elements :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Output = %d\n",count(arr,size));
}
