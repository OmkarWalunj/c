//reverse in  range
//
//problem Description
//-GIven an array A and an N integer
//-reverse the array a after reversing in the  another array

#include<stdio.h>

int* reverse(const int * arr,int * arr1,int size){
	int i=0;
	while(size !=0){
		arr1[size-1] = arr[i];
		i++;
		size--;
	}
	return arr1;
}
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter array Elements :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int arr1[size];
	int * ptr=reverse(arr,arr1,size);

	printf("Reverse array :\n");

	for(int i=0 ;i<size;i++){
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}
