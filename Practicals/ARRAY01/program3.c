//reverse in  range
//
//problem Description
//-GIven an array A and an N integer 
//Also given are two integer b and c
//-reverse the array a after reversing in the  given range

#include<stdio.h>

int* reverse(int * arr,int size,int b , int c){
	int a;
	while(b<c){
		a=arr[b];
		arr[b]=arr[c];
		arr[c]=a;
		b++;
		c--;
	}
	return arr;
}
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter array Elements :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int B;
	printf("Enter inter B:\n");
	scanf("%d",&B);
	int C;
        printf("Enter inter C:\n");
        scanf("%d",&C);
	int * ptr=reverse(arr,size,B,C);

	printf("Reverse array :\n");

	for(int i=0 ;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
