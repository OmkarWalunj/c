/*   1 
 *   1  2  3
 *   1  2  3  4  5
 *   1  2  3
 *   1
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols;

	for (int i=1;i<2*rows;i++){
		int num=1;
		if(i<=rows){
			cols=2*i-1;
		}else{
			cols=cols-2;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}

