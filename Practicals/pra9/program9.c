/*   1
 *   3  2  1
 *   5  4  3  2  1
 *   3  2  1
 *   1
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){

		if(i<=rows){
			cols=2*i-1;
			num=2*i-1;
		}else{
			cols=cols-2;
			num=3*rows-2*i+rows-1;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num--;
		}
		printf("\n");
	}
}

