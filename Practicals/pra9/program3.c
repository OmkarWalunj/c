/*   1 
 *   2  1
 *   3  2  1
 *   4  3  2  1
 *   3  2  1 
 *   2  1
 *   1
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=i;
			cols=i;
		}else{
		        num=2*rows-i;
			cols=2*rows-i;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num--;
		}
		printf("\n");
	}
}

