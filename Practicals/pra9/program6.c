/*   * 
     *  *  *
     *  *  *  *  *
     *  *  *
     *
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols;

	for (int i=1;i<2*rows;i++){

		if(i<=rows){
			cols=2*i-1;
		}else{
			cols=cols-2;
		}
		for (int j=1;j<=cols;j++){
			printf("*\t");
		}
		printf("\n");
	}
}

