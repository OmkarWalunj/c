/*   3 
 *   2  3
 *   1  2  3
 *   0  1  2  3
 *   1  2  3
 *   2  3
 *   3
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			cols=i;
			num=rows-i;
		}else{
			cols=2*rows-i;
			num=i-rows;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}

