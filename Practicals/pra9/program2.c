/*   1 
 *   1  2
 *   1  2  3
 *   1  2  3  4
 *   1  2  3 
 *   1  2
 *   1
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols;

	for (int i=1;i<2*rows;i++){
		int num=1;
		if(i<=rows){
			cols=i;
		}else{
			cols=2*rows-i;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}

