/*   D 
 *   C  D
 *   B  C  D
 *   A  B  C  D
 *   B  C  D
 *   C  D
 *   D
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			cols=i;
			num=64+rows-i+1;
		}else{
			cols=2*rows-i;
			num=64+i-rows+1;
		}
		for (int j=1;j<=cols;j++){
			printf("%c\t",num);
			num++;
		}
		printf("\n");
	}
}

