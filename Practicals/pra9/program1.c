/*   * 
 *   *  *
 *   *  *  *
 *   *  *  *  *
 *   *  *  * 
 *   *  *
 *   *
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols;

	for (int i=1;i<2*rows;i++){

		if(i<=rows){
			cols=i;
		}else{
			cols=2*rows-i;
		}
		for (int j=1;j<=cols;j++){
			printf("*\t");
		}
		printf("\n");
	}
}

