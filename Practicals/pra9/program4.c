/*   4 
 *   3  3
 *   2  2  2
 *   1  1  1  1
 *   2  2  2
 *   3  3
 *   4
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			cols=i;
			num=rows-i+1;
		}else{
			cols=2*rows-i;
			num=i-rows+1;
		}
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
		}
		printf("\n");
	}
}

