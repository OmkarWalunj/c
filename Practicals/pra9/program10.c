/*   1 
 *   2  4
 *   3  6  9
 *   4  8  12  16
 *   3  6  9 
 *   2  4
 *   1
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=i;
			cols=i;
		}else{
		        num=2*rows-i;
			cols=2*rows-i;
		}
		for (int j=1;j<=cols;j++){
			if (i<=rows){
				printf("%d\t",num);
				num=num+i;
			}else{
				printf("%d\t",num);
				num=num+2*rows-i;
			}
		}
		printf("\n");
	}
}

