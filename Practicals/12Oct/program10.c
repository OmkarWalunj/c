/*	10
 *	I    H
 *	7    6   5
 *	D    C   B  A
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	int num = rows*(rows+1)/2;
	for (int i=1;i<=rows;i++){
		for (int j=1;j<=i;j++){
			if (i%2==0){
				printf("%d\t",num);
				num--;
			}else{
				printf("%c\t",num+64);
				num--;
			}
		}
		printf("\n");
	}
}
