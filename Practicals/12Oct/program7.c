/*	d
 *	c   c
 *	b   b   b
 *	a   a   a  a
 */

#include <stdio.h>

void main(){

	int rows ,ch;
	printf("Enter rows:");
	scanf("%d",&rows);
	ch = 96+rows;
	for (int i =1;i<=rows;i++){
		for (int j=1;j<=i;j++){
			printf("%c\t",ch);
		}
		ch--;
		printf("\n");
	}
}
