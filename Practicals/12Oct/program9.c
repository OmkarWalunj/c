/*	3
 *	6   9
 *	12  15   18
 */

#include <stdio.h>

void main(){

	int rows ,ch,num=1;
	printf("Enter rows:");
	scanf("%d",&rows);
	ch = rows;
	for (int i =1;i<=rows;i++){
		for (int j=1;j<=i;j++){
			printf("%d\t",ch*num);
			num++;
		}
		printf("\n");
	}
}
