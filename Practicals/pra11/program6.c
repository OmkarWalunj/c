/*		A
 *	    b   b   b
 *	C   C   C   C   C
 *	    b   b   b
 *	        A
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=64+i;
			sp=rows-i;
			cols=2*i-1;
		}else{
			num=64+2*rows-i;
			sp=i-rows;
			cols=cols-2;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			if(i%2==0){
				printf("%c\t",num+32);
			}else{
				printf("%c\t",num);
			}
		}
		printf("\n");
	}
}
