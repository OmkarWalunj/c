/*		1
 *	    2   1   2
 *	3   2   1   2   3
 *	    2   1   2
 *	        1
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols,num;

	for (int i=1;i<2*rows;i++){
		int num=1;
		if(i<=rows){
			num=i;
			sp=rows-i;
			cols=2*i-1;
		}else{
			sp=i-rows;
			cols=cols-2;
			num=2*rows-i;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			if(i<=rows){
				if (j<i){
					printf("%d\t",num);
					num--;
				}else{
					printf("%d\t",num);
					num++;
				}
			}else{
				if(j<2*rows-i){
					printf("%d\t",num);
					num--;
				}else{
					printf("%d\t",num);
                                        num++;
				}
			}
		}
		printf("\n");
	}
}
