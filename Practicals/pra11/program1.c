/*		*
 *	    *   *   *
 *	*   *   *   *   *
 *	    *   *   *
 *	        *
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			sp=rows-i;
			cols=2*i-1;
		}else{
			sp=i-rows;
			cols=cols-2;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			printf("*\t");
		}
		printf("\n");
	}
}
