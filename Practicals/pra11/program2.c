/*		1
 *	    2   2   2
 *	3   3   3   3   3
 *	    2   2   2
 *	        1
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=i;
			sp=rows-i;
			cols=2*i-1;
		}else{
			num=2*rows-i;
			sp=i-rows;
			cols=cols-2;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			printf("%d\t",num);
		}

		printf("\n");
	}
}
