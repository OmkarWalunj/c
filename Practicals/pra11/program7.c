/*		A
 *	    b   A   b
 *	C   b   A   b   C
 *	    b   A   b
 *	        A
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols,num;

	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=64+i;
			sp=rows-i;
			cols=2*i-1;

		}else{
			sp=i-rows;
			cols=cols-2;
			num=64+2*rows-i;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			if(i<=rows){
				if (j<i){
					if(num%2==0){
						printf("%c\t",num+32);
					}else{
						printf("%c\t",num);
					}
					num--;
				}else{
					 if(num%2==0){
                                                printf("%c\t",num+32);
                                        }else{
                                                printf("%c\t",num);
                                        }
                                        num++;

				}
			}else{
				if(j<2*rows-i){
					 if(num%2==0){
                                                printf("%c\t",num+32);
                                        }else{
                                                printf("%c\t",num);
                                        }
                                        num--;

				}else{
					 if(num%2==0){
                                                printf("%c\t",num+32);
                                        }else{
                                                printf("%c\t",num);
                                        }
                                        num++;

				}
			}
		}
		printf("\n");
	}
}
