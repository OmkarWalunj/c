/*	*  *  *  *  *  *  *
 *	*  *  *     *  *  *
 *	*  *           *  *
 *	*                 *
 *	*  *           *  *
 *	*  *  *     *  *  *
 *	*  *  *  *  *  *  *
 */

#include <stdio.h>

void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	int star,cols,sp;
	for (int i=1;i<2*rows;i++){

		if(i<=rows){
			star = rows-i+1;
			cols= rows-2+i;
			sp=2*i-1;
		}else{
			star=star+1;
			cols=cols-1;
			sp=sp-2;
		}
		for(int j=1;j<=star;j++){
			printf("*  ");
		}
		for(int k=1;k<=cols;k++){
			if(k<=sp-2){
				printf("   ");
			}else{
				printf("*  ");
			}
		}
		printf("\n");
	}
}
