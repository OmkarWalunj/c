/*		C
 *	    C   B   C
 *	C   B   A   B   C
 *	    C   B   C
 *	        C
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter No of rows:");
	scanf("%d",&rows);
	int sp,cols;

	for (int i=1;i<2*rows;i++){
		int num=64+rows;
		if(i<=rows){
			sp=rows-i;
			cols=2*i-1;
		}else{
			sp=i-rows;
			cols=cols-2;
		}
		for (int space=1;space<=sp;space++){
			printf(" \t");
		}
		for ( int j=1;j<=cols;j++){
			if(i<=rows){
				if (j<i){
					printf("%c\t",num);
					num--;
				}else{
					printf("%c\t",num);
					num++;
				}
			}else{
				if(j<2*rows-i){
					printf("%c\t",num);
					num--;
				}else{
					printf("%c\t",num);
                                        num++;
				}
			}
		}
		printf("\n");
	}
}
