/*
 * program 6.
 * WAP that accept  a singly linked list from the user take a number from the user and revese the data elements from the linked list
 * input linked list :|shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|
 * input linked list : |ihsahs|->
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	char name[10];
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	
	getchar();
	printf("Enter name:\n");
	char ch;
	int i=0;
	while((ch=getchar()) != '\n'){
		newnode ->name[i]=ch;
		i++;
	}

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int revstr(){
	node * temp = head;
	if(head == NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		
		while( temp != NULL){
			char *str1=temp ->name;
			char * ptr1=str1;
			while(*ptr1 !='\0'){
				ptr1++;
			}
			ptr1--;
			char ch2;
			int i=0;
			while(str1<ptr1){
				ch2=*str1;
				*str1 = *ptr1;
				*ptr1=ch2;
				temp->name[i]=*str1;
				str1++;
				ptr1--;
				i++;
			}
			temp = temp ->next;
		}
		return 0;
	}
}
void printLL(){
	node * temp = head;
	if(head == NULL){
		printf("Linked list is empty \n");

	}else{
		while(temp->next != NULL){
			printf("|%s|->",temp ->name);
			temp = temp ->next;
		}
		printf("|%s|\n",temp -> name);
	}
}
void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}

	revstr();
	printLL();
}	
