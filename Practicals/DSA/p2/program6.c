/*
 * program 6.
 * WAP that accept  a singly linked list from the user take a number from the user and print the data of the length of that number
 * input linked list :|shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|
 * input  : Enter Length :5
 * Output:
 * Kanha
 * Rahul
 * Badhe
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	char name[10];
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	
	getchar();
	printf("Enter name:\n");
	char ch;
	int i=0;
	while((ch=getchar()) != '\n'){
		newnode ->name[i]=ch;
		i++;
	}

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int Lenstr(){
	int input;
	printf("Enter length of str:\n");
	scanf("%d",&input);

	node * temp = head;
	if(head == NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		
		while( temp != NULL){
			char * ptr=(temp ->name);
			int count=0;
			while(*ptr !='\0'){
				count++;
				ptr++;
			}
			if (input == count){
				printf("%s\n",temp->name);
			}
			temp = temp ->next;
		}
		return 0;
	}
}

void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}

	Lenstr();
}	
