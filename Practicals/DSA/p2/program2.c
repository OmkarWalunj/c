/*
 * program 2.
 * WAP that searches for the second last occurence of a particular element from a singly linked list.
 * input linked list :|10|->|20|->|30|->|40|->|50|->|30|
 * input : Enter element :30
 * output : 3
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));

	getchar();
	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int SecondLastOcc(){
	int input;
	printf("Enter Number:\n");
	scanf("%d",&input);

	node * temp = head;
	int count =0,count1=0,prev=0;
	if(head = NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		while( temp != NULL){
			count++;
			if(input == temp ->data){
				prev = count1;
				count1=count;	
			}
			temp = temp ->next;
		}
		printf("%d\n",prev);
		return 0;
	}
}

void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}
	SecondLastOcc();
}
		
