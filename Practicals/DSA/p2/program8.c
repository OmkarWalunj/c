//create same length of str data in linked list other delete
#include <stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
	char name[20];
	struct node * next;
}node;

node *head=NULL;

node *createnode(){
	node * newnode=(node*)malloc(sizeof(node));

	getchar();
	printf("Enter name:\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		newnode->name[i]=ch;
		i++;
	}

	newnode->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();
	if(head == NULL){
		head=newnode;
	}else{
		node * temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int countnode(){
	int count=0;
	node * temp=head;
	while(temp !=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

int printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		node *temp=head;
		while(temp->next != NULL){
			printf("|%s|->",temp->name);
			temp=temp->next;
		}
		printf("|%s|\n",temp->name);
		return 0;
	}
}

int deleteFirst(){
	
	if(head==NULL){
		printf("Linked List is Empty\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			
			node *temp=head;
			head=temp->next;
			free(temp);
		}
		return 0;
	}
}
int deleteLast(){

	if(head==NULL){
		printf("Linked List is Empty\n");
		return -1;
	}else{
		if(head ->next==NULL){
			deleteFirst();
		}else{
			node *temp=head;
			while(temp->next->next !=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
		return 0;
	}
}
int deleteAtPos(int pos){
	int count=countnode();
	if(pos<=0 || pos> count){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos ==1){
			deleteFirst();
		}else if( pos==count){
			deleteLast();
		}else{
			node * temp=head;
			node *temp1=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp1=temp->next->next;
			free(temp->next);
			temp->next=temp1;
		}
		return 0;
	}
}

int Lenstr(){
	int input;
	printf("Enter Length:\n");
	scanf("%d",&input);

	if(head==NULL){
		printf("Linked List is Empty\n");
		return -1;
	}else{
		
		node *temp=head;
		int count=0;
		while(temp != NULL){
			count++;
			int len=strlen(temp->name);
			if(input !=len){
				deleteAtPos(count);
				count--;
			}
			temp=temp->next;
		}
	}
}
void main(){
	int node;
	printf("Enter No of Nodes:\n");
	scanf("%d",&node);

	for(int i=0;i<node;i++){
		addnode();
	}
	Lenstr();
	printLL();
}




