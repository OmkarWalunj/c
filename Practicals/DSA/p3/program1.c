/* Doubly Linked list structure :
 * struct node{
 * 	int data;
 * 	struct node *next;
 * }
 *
 * program 1.
 * WAP that searches for the first occurence of a particular element from a singly linked list.
 * input linked list :|10|->|20|->|30|->|40|->|50|->|30|
 * input : Enter element :30
 * output : 5
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	newnode ->prev = NULL;
	getchar();
	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
		temp->prev=temp;
	}
}

int FirstOcc(){
	int input;
	printf("Enter Number:\n");
	scanf("%d",&input);

	node * temp = head;
	int count =0;
	if(head = NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		while( temp != NULL){
			count++;
			if(input == temp ->data){
				printf("%d\n",count);
				break;
			}
			temp = temp ->next;
		}
		return 0;
	}
}

void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}
	FirstOcc();
}
		
