/*
 * program 5.
 * WAP that searches all the palindrome data elements from a doubly linked list.and print the position of palindeome data
 * input linked list :|12|->|121|->|30|->|252|->|35|->|151|->|70|
 * output : palindrom found at 2
 *  palindrom found at 4 
 *  palindrom found at 6
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node * prev;
	int data;
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	newnode->prev=NULL;
	getchar();
	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
		temp->prev=temp;
	}
}

int palindrom(){
	node * temp = head;
	if(head == NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		int count=0;
		while( temp != NULL){
			count++;
			int rem=0;
			int sum=0;
			int num=temp->data;
			while(num !=0){
				rem=num %10;
				sum=sum*10+rem;
				num = num/10;
			}
			if ( sum == temp ->data){
				printf("Palindrome Found at %d\n",count);
			}
			temp = temp ->next;
		}
		return 0;
	}
}

void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}

	palindrom();
}	
