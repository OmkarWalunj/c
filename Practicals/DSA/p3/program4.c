/*
 * program 4.
 * WAP that searches add the digit of a data elements from a doubly linked list.
 * input linked list :|10|->|20|->|30|->|44|->|50|->|30|
 * output : |1|->|2|->|3|->|8|
 */

#include <stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node * prev;
	int data;
	struct node *next;
}node;

node * head=NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	newnode ->prev=NULL;
	getchar();
	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode ->next =NULL;

	return newnode;
}

void addnode(){
	node * newnode =createnode();

	if(head == NULL){
		head =newnode;
	}else{
		node * temp=head;

		while(temp -> next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
		temp->prev=temp;
	}
}

int adddigit(){
	node * temp = head;
	if(head == NULL){
		printf("Linked list empty\n");
		return -1;
	}else{
		while( temp != NULL){
			int rem=0;
			int sum=0;
			while(temp->data !=0){
				rem=temp->data %10;
				sum=sum+rem;
				temp->data = temp->data/10;
			}
			temp->data=sum;
			temp = temp ->next;
		}
		return 0;
	}
}
void printLL(){
	node * temp = head;
	if(head == NULL){
		printf("Linked list is empty \n");

	}else{
		while(temp->next != NULL){
			printf("|%d|->",temp ->data);
			temp = temp ->next;
		}
		printf("|%d|\n",temp -> data);
	}
}

void main(){
	int node ;
	printf("Enter No of Node:\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}

	adddigit();
	printLL();
}	
