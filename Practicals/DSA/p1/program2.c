/* WAP FOR THE LINKED LIST OF STATE OF INDIA  CONSISTING OF ITS NAME ,POPULATION  AND BUDGET  CONNECTS 4 STATES IN THE LINKED LIST AND PRINT THEIR DATA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct states{
	char sname[20];
	int population;
	float Budget;
	struct states * next ;
}state;

state * head = NULL;

void addnode(){
	state * newnode = (state *)malloc(sizeof(state));

	printf("Enter name of state: \n");
	fgets(newnode -> sname , 15,stdin);

	printf("Enter population of state : \n");
	scanf("%d",&(newnode -> population));
	
	printf("Enter Budget :\n");
	scanf("%f",&(newnode -> Budget));

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		state * temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp ->next = newnode;
	}
}

void printLL(){
	state * temp = head;

	while ( temp != NULL){

		temp -> sname[strlen(temp ->sname)-1]= '\0';
		printf("|%s -> ",temp -> sname);
		printf("%d -> ",temp -> population);
		printf("%f |",temp -> Budget);

		temp = temp -> next;
	}
}

void main(){

	addnode();
	addnode();
	addnode();

	printLL();
	printf("\n");
}
