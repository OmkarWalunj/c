/* WAP FOR THE LINKED LIST OF MALLS CONSISTING OF ITS NAME ,NUMBER OF SHOPS . AND REVENUE CONNECTS MALLS IN THE LINKED LIST AND PRINT THEIR DATA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct malls{
	char mname[20];
	int nshops;
	float revenue;
	struct malls * next ;
}malls;

malls * head = NULL;

void addnode(){
	malls * newnode = (malls *)malloc(sizeof(malls));

	printf("Enter name of malls: \n");
	fgets(newnode -> mname , 15,stdin);

	printf("Enter no of shops : \n");
	scanf("%d",&(newnode -> nshops));
	
	printf("Enter revenue :\n");
	scanf("%f",&(newnode -> revenue));

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		malls * temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp ->next = newnode;
	}
}

void printLL(){
	malls * temp = head;

	while ( temp != NULL){

		temp -> mname[strlen(temp ->mname)-1]= '\0';
		printf("|%s -> ",temp -> mname);
		printf("%d -> ",temp -> nshops);
		printf("%f |",temp -> revenue);

		temp = temp -> next;
	}
}

void main(){

	addnode();
	addnode();
	addnode();

	printLL();
	printf("\n");
}
