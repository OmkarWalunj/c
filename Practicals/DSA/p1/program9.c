/* Write a demo structure consisting of integer data take the number of nodes from user and to check the prime number present in data ;*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;

Demo * head = NULL ;

void addnode(){

	Demo * newnode = (Demo * )malloc(sizeof(Demo));

	printf ("Enter Number :\n");
	scanf("%d", &(newnode -> data));

	newnode -> next =NULL;

	if ( head == NULL){

		head = newnode ;
	}else{
		Demo * temp =head;
		
		while( temp -> next != NULL) {
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void prime(){
	int flag1 =0,flag2=0;
	Demo * temp = head;

	while( temp != NULL){
		int count =0;
		int prime = 0;
		for (int i =2;i<= temp ->data;i++){
			if( temp -> data % i == 0){
				count++;
			}
		}
		if(count == 1){
			prime=temp->data;
			printf("%d ",prime);
		}else{
			flag1++;
		}
		temp = temp -> next;
		flag2++;
	}
	if( flag1 == flag2){
		printf( "There is no prime number");
	}

}

void main(){

	int node;
	printf("Enter no of nodes : \n");
	scanf("%d",&node);

	for (int i=0; i<node;i++){
		addnode();
	}
	printf("prime no are :\n");
	prime();
	printf("\n");
}

