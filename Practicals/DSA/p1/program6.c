/* Write a demo structure consisting of integer data take the number of nodes from user and print the addition of the first and last node ;*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;

Demo * head = NULL ;

void addnode(){

	Demo * newnode = (Demo * )malloc(sizeof(Demo));

	printf ("Enter Number :\n");
	scanf("%d", &(newnode -> data));

	newnode -> next =NULL;

	if ( head == NULL){

		head = newnode ;
	}else{
		Demo * temp =head;
		
		while( temp -> next != NULL) {
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

int add(){
	int add=0;
	Demo * temp = head;

	while( temp != NULL){
		if( temp ->next ==NULL){
			add=head -> data +temp -> data;
		}
		temp = temp -> next;
	}

	return add;
}

void main(){

	int node;
	printf("Enter no of nodes : \n");
	scanf("%d",&node);

	for (int i=0; i<node;i++){
		addnode();
	}

	printf(" Addition of first and last number is : %d\n",add());
}

