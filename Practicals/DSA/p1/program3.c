/* WAP FOR THE LINKED LIST OF FESTIVAL IN INDIA TAKE INPUT FROM USER IN THE LINKED LIST AND PRINT THEIR DATA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Fest{
	char fname[20];
	int nitem_menu;
	float Budget_menu;
	struct Fest * next ;
}Fest;

Fest * head = NULL;

void addnode(){
	Fest * newnode = (Fest *)malloc(sizeof(Fest));

	printf("Enter name of Festival: \n");
	fgets(newnode -> fname , 15,stdin);

	printf("Enter no of items in menu : \n");
	scanf("%d",&(newnode -> nitem_menu));
	
	printf("Enter Budget of menu :\n");
	scanf("%f",&(newnode -> Budget_menu));

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		Fest * temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp ->next = newnode;
	}
}

void printLL(){
	Fest * temp = head;

	while ( temp != NULL){

		temp -> fname[strlen(temp ->fname)-1]= '\0';
		printf("|%s -> ",temp -> fname);
		printf("%d -> ",temp -> nitem_menu);
		printf("%f |",temp -> Budget_menu);

		temp = temp -> next;
	}
}

void main(){

	addnode();
	addnode();
	addnode();

	printLL();
	printf("\n");
}
