/* Write a real time example for a linked list and print its data */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Bank{
	char bname[20];
	int naccounts;
	float investment;
	struct Bank * next ;
}Bank;

Bank * head = NULL;

void addnode(){
	Bank * newnode = (Bank *)malloc(sizeof(Bank));
	getchar();
	printf("Enter name of Bank: \n");
	fgets(newnode -> bname , 15,stdin);

	printf("Enter no of accounts : \n");
	scanf("%d",&(newnode -> naccounts));
	
	printf("Enter yearly investment :\n");
	scanf("%f",&(newnode -> investment));

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		Bank * temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp ->next = newnode;
	}
}

void printLL(){
	Bank * temp = head;

	while ( temp != NULL){

		temp -> bname[strlen(temp ->bname)-1]= '\0';
		printf("|%s -> ",temp -> bname);
		printf("%d -> ",temp -> naccounts);
		printf("%f |",temp -> investment);

		temp = temp -> next;
	}
}

void main(){
	int node;
	printf("Enter no node :\n");
	scanf("%d",&node);

	for ( int i=0; i<node ; i++){
		addnode();
	}
	printLL();
	printf("\n");
}
