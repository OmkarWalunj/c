/* Find out middle node of Linked List */

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;
node *head=NULL;
int flag=0;
node *createnode(){
	node *newnode=(node *)malloc(sizeof(node));

	printf("Enter Data:\n");
	scanf("%d",&newnode->data);

	newnode ->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
	}else{
		node *temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("\n");
}
int countnode(){
	struct node *temp=head;
	int count=0;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

int MiddleNode1(){
	if(head==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		if(head->next == NULL){
			return head->data;
		}else{
			int cnt=countnode()/2;
			int mid=cnt+1;
			struct node * temp=head;
			while(cnt){	
				temp=temp->next;
				cnt--;
			}
			return temp->data;
		}
	}
}

int MiddleNode2(){
	if(head==NULL){
                flag=0;
                return -1;
        }else{
                flag=1;
                if(head->next == NULL){
                        return head->data;
                }else{
			struct node * fastptr=head;
			struct node * slowerptr=head;
			while(fastptr !=NULL && fastptr->next !=NULL){
				fastptr=fastptr->next ->next;
				slowerptr=slowerptr->next;
			}
			return slowerptr->data;
		}
	}
}
void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);

	for(int i=0;i<node;i++){
		addnode();
	}
	printLL();
	int data=MiddleNode1();
	if(flag ==0){
		printf("Linked List is Empty\n");
	}else{
		printf("Middle Node Element:%d\n",data);
	}
	int data1=MiddleNode2();
        if(flag ==0){
                printf("Linked List is Empty\n");
        }else{
                printf("Middle Node Element:%d\n",data1);
        }


}
