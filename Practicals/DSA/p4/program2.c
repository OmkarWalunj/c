/* implace reverse program in doubly linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;
node *head=NULL;
node *createnode(){
	node *newnode=(node *)malloc(sizeof(node));

	newnode ->prev=NULL;
	printf("Enter Data:\n");
	scanf("%d",&newnode->data);

	newnode ->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
	}else{
		node *temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->prev=temp;
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("\n");
}
void rev(){

	node *temp=NULL;
	while(head !=NULL){
		head->prev=head->next;
		head->next=temp;
		temp=head;
		head=head->prev;
	}
	head=temp;
}

void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);

	for(int i=0;i<node;i++){
		addnode();
	}
	printLL();
	rev();
	printLL();
}
