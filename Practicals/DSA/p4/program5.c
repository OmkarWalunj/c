/* implace reverse program in singly circular linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;
node *head=NULL;
node *createnode(){
	node *newnode=(node *)malloc(sizeof(node));

	printf("Enter Data:\n");
	scanf("%d",&newnode->data);

	newnode ->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
		newnode->next=head;
	}else{
		node *temp=head;
		while(temp->next !=head){
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->next=head;
	}
}
void printLL(){
	node *temp=head;
	while(temp->next!=head){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
	printf("\n");
}
void rev(){

	node *temp=NULL;
	node *temp1=NULL;
	node *temp2=head;
	while(temp1 !=head){
		temp1=temp2->next;
		temp2->next=temp;
		temp=temp2;
		temp2=temp1;
	}
	head->next=temp;
	head=temp;
}

void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);

	for(int i=0;i<node;i++){
		addnode();
	}
	printLL();
	rev();
	printLL();
}
