/* implace reverse program in doubly circular linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;
node *head=NULL;
node *createnode(){
	node *newnode=(node *)malloc(sizeof(node));

	newnode ->prev=NULL;
	printf("Enter Data:\n");
	scanf("%d",&newnode->data);

	newnode ->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
		head->prev=head;
		head->next=head;
	}else{
		node *temp=head;
		while(temp->next !=head){
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->prev=temp;
		newnode->next=head;
		head->prev=newnode;
	}
}
void printLL(){
	node *temp=head;
	while(temp->next!=head){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
}
void rev(){

	node *temp1=NULL;
	node *temp2=head;
	while(temp1 !=head){
		temp1=temp2->next;
		temp2->next=temp2->prev;
		temp2->prev=temp1;
		temp2=temp1;
	}
	head=head->next;
}

void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);

	for(int i=0;i<node;i++){
		addnode();
	}
	printLL();
	rev();
	printLL();
}
