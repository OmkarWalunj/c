//concat last n node of linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head1=NULL;
node *head2=NULL;

node *createnode(){
	node *newnode=(node *)malloc (sizeof(node));

	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;
	return newnode;
}

void addnode(node ** head){
	node *newnode=createnode();
	if(*head==NULL){
		*head=newnode;
	}else{
		node *temp=*head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printLL(node *head){
	if(head==NULL){
		printf("Linked list is Empty\n");
	}else{
		node *temp=head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
int countnode(node *head){
	int count=0;
	while(head !=NULL){
		count++;
		head=head->next;
	}
	return count;
}

int concat_Last_N_node(int num){
	int count=countnode(head2);
	if(num<=0 || num>count){
		printf("can not concat linked list\n");
		return -1;
	}else{
		int cnt=count-num;
		node *temp1=head1;
		while(temp1->next !=NULL){
			temp1=temp1->next;
		}
		node *temp2=head2;
		while(cnt){
			temp2=temp2->next;
			cnt--;
		}
		temp1->next=temp2;
	}
}

void main(){
	int countnode;
	printf("Enter node:linked list1:\n");
	scanf("%d",&countnode);

	for(int i=0;i<countnode;i++){
		addnode(&head1);
	}

	printf("Enter node:linked list2:\n");
	scanf("%d",&countnode);

	for(int i=0;i<countnode;i++){
		addnode(&head2);
	}
	printLL(head1);
	printLL(head2);
	int num;
	printf("Enter number:\n");
	scanf("%d",&num);
       	concat_Last_N_node(num);
	printLL(head1);
}








