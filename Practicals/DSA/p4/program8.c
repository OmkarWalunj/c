/* Find linked list is palindrome or/not */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Demo{

	int data ;
	struct Demo * next;
}Demo;

int flag=0;
Demo *head =NULL;

Demo * createnode(){
	Demo * newnode = (Demo *)malloc(sizeof(Demo));
	
	printf("Enter Data :\n");
	scanf("%d",&newnode -> data);

	newnode -> next =NULL;

	return newnode;
}

void addnode(){

	Demo * newnode=createnode();

	if(head == NULL){
		head = newnode;
	}else {
		Demo * temp = head;

		while(temp ->next != NULL){
			temp =temp ->next;
		}
		temp ->next =newnode;
	}
}
int count(){
	Demo * temp =head;
	
	int count =0;
	while( temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

bool ispalindrome1(){
	if(head==NULL){
		return false;
	}else{
		if(head->next == NULL){
			return true;
		}else{
			int cnt=count();
			int arr[cnt];
			Demo * temp=head;
			int i=0;
			while(temp !=NULL){
				arr[i]=temp->data;
				temp=temp->next;
				i++;
			}
			int start=0;
			int end=cnt-1;
			while(start < end){
				if(arr[start] != arr[end]){
					return false;
				}
				start++;
				end++;
			}
			return true;
		}
	}
}
Demo * MiddleNode(){
	if(head==NULL){
                flag=0;
        }else{
                flag=1;
                if(head->next == NULL){
                        return head;
                }else{
			struct Demo * fastptr=head;
			struct Demo * slowerptr=head;
			while(fastptr !=NULL && fastptr->next !=NULL){
				fastptr=fastptr->next ->next;
				slowerptr=slowerptr->next;
			}
			return slowerptr;
		}
	}
}
bool ispalindrome2(){
        if(head==NULL){
                return false;
        }else{
                if(head->next == NULL){
                        return true;
                }else{
                        int cnt=count()/2;
			int mid=cnt+1;
                        Demo * temp=MiddleNode();
			temp=temp->next;
			Demo *temp1=NULL;
			Demo *temp2=NULL;
			while(temp != NULL){
				temp2=temp->next;
				temp->next=temp1;
				temp1=temp;
				temp=temp2;
			}
			temp=temp1;
			Demo * start=head;
			Demo * end=temp;
			bool ret;
			while(mid-1){
				if(start->data != end->data){
					ret=false;
					break;
				}else{
					ret=true;
				}
				start=start->next;
				end=end->next;
				mid--;
			}
			temp1=NULL;
                        while(temp != NULL){
                                temp2=temp->next;
                                temp->next=temp1;
                                temp1=temp;
                                temp=temp2;
                        }
                        temp=temp1;
                        return ret;
                }
        }
}

void addFirst(){
	Demo * newnode =createnode();

	if(head ==NULL){
		head = newnode;
	}else{
		newnode ->next = head;
		head =newnode;
	}
}

int addAtPos(){
	int pos;
	printf("\nEnter Position of node you have to add :\n");
	scanf("%d",&pos);
	
	int Count = count();
	if(pos <=0  || pos >= Count+2){
		printf("Invalid position \n");
		return -1;
	}else {
		if(pos == 1){
			addFirst();
		}else if(pos == Count+1){
			addnode();
		}else{

			Demo * newnode =createnode();
			Demo * temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			newnode ->next = temp ->next;
			temp->next = newnode;
		}
		return 0;
	}
}

void addLast(){
	addnode();
}

void printLL(){
	Demo * temp = head;
	if(head==NULL){
		printf("Linked List is Empty \n");
	}else{
		while(temp !=NULL){
			printf("|%d|",temp-> data);
			temp = temp-> next;
		}
		printf("\n");
	}
}

void deleteFirst(){

	Demo * temp = head;
	head= temp -> next;
	free(temp);
}

void deleteLast(){
	int Count = count();
	Demo * temp = head;
	if(Count == 1){
		head = NULL;
	}else if (Count > 1){
		while(temp -> next-> next != NULL){
			temp=temp -> next;
		}
		free(temp -> next);
		temp -> next =NULL;
	}else{
		printf("Linked is Empty\n");
	}
}
int deleteAtPos(){
        int pos;
        printf("\nEnter Position of node you have delete :\n");
        scanf("%d",&pos);

        int Count = count();
        if(pos <= 0 || pos > Count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos == Count ){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{

                	Demo * temp = head;
			Demo * temp1 ;
                	while(pos - 2){
                        	temp = temp->next;
                        	pos--;
                	}
                	temp1 = temp ->next -> next;
			free(temp ->next);
			temp ->next = temp1;
		}
		return 0;
	}
}
void rev(){

	Demo *temp=NULL;
	Demo *temp1=NULL;
	while(head !=NULL){
		temp1=head->next;
		head->next=temp;
		temp=head;
		head=temp1;
	}
	head=temp;
}
void main(){

	char choice;
	do{
		printf(" ****************** Main *************\n");
		printf(" 1.addnode\n 2.addFirst\n 3.addAtPos\n 4.addAtLast\n 5.count\n 6.printLL\n 7.deleteFirst\n 8.deleteLast\n 9.deleteAtPos\n 10.isPalindrome:Approch1\n 11.isPalinrome:Approch2\n 12.MiddleNode\n 13.reverse\n");

		int ch;
		printf("Enter Your choice : \n");
		scanf("%d",&ch);

		switch (ch){
                	case 1:
                        	addnode();
                        	break;
                	case 2:
                        	addFirst();
                        	break;
                	case 3:
                        	addAtPos();
                        	break;
                	case 4:
                        	addLast();
                        	break;
                	case 5:
				{
                        		int Count=count();
					printf("Count : %d\n",Count);
				}
                       		break;
                	case 6:
                        	printLL();
                        	break;
			case 7:
				deleteFirst();
				break;
			case 8:
				deleteLast();
				break;
			case 9:
				deleteAtPos();
				break;
			case 10:{
					bool ret=ispalindrome1();
					if(ret == false){
						printf("Not Palindrome \n");
					}else{
						printf("Is Palindrome \n");
					}
				}
				break;
				
			 case 11:{
                                        bool ret=ispalindrome2();
					if(ret == false){
                                                printf("Not Palindrome \n");
                                        }else{
                                                printf("Is Palindrome \n");
                                        }
				 }
				 break;
			case 12:{
					Demo * ptr=MiddleNode();
        				if(flag ==0){
                				printf("Linked List is Empty\n");
      					}else{
               					printf("Middle Node Element:%d\n",ptr->data);
 				        }
				}
				break;
			case 13:
				rev();
				break;

                	default :
                        	printf("Wrong Input\n");
				break;
        	}
		
		getchar();
		printf("Do you have continue:\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

     


	

