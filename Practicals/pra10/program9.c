/*		1
 *  	     2  4
 *        3  6  9
 *     4  8  12 16
 *        3  6  9
 *           2  4
 *              1
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=i;
			sp=rows-i;
			cols=i;
		}else{
			num=2*rows-i;
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			if(i<=rows){
				printf("%d\t",num);
				num=num+i;
			}else{
				printf("%d\t",num);
				num=num+2*rows-i;
			}
		}
		printf("\n");
	}
}
