/*		1
 *  	     1  2
 *  	  2  3  4
 *    4   5  6  7
 *        7  8  9
 *           9  10
 *              10
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num=1;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			sp=rows-i;
			cols=i;
		}else{
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		num--;
		printf("\n");
	}
}
