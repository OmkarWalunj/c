/*		1
 *  	     1  2
 *  	  1  2  3
 *     1  2  3  4
 *        1  2  3
 *           1  2
 *              1
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols;
	for (int i=1;i<2*rows;i++){
		int num=1;
		if(i<=rows){
			sp=rows-i;
			cols=i;
		}else{
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}
