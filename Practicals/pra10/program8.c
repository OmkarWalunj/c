/*		D
 *  	     C  D
 *        B  C  D
 *     A  B  C  D
 *        B  C  D
 *           C  D
 *              D
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=64+rows-i+1;
			sp=rows-i;
			cols=i;
		}else{
			num=64+i-rows+1;
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%c\t",num);
			num++;
		}
		printf("\n");
	}
}
