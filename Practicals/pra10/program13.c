/*		A
 *  	     a  b
 *        B  C  D
 *     d  e  f  g
 *        G  H  I
 *           i  j
 *              J
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num=65;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			sp=rows-i;
			cols=i;
		}else{
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			if (i%2==0){
				printf("%c\t",num+32);
			}else{
				printf("%c\t",num);
			}
			num++;
		}
		num--;
		printf("\n");
	}
}
