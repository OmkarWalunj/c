/*		4
 *  	     3  3
 *  	  2  2  2
 *     1  1  1  1
 *        2  2  2
 *           3  3
 *              4
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=rows-i+1;
			sp=rows-i;
			cols=i;
		}else{
			num=i-rows+1;
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%d\t",num);
		}
		printf("\n");
	}
}
