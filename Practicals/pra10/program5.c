/*		3
 *  	     2  3
 *        1  2  3
 *     0  1  2  3
 *        1  2  3
 *           2  3
 *              3
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=rows-i;
			sp=rows-i;
			cols=i;
		}else{
			num=i-rows;
			sp=i-rows;
			cols=2*rows-i;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}
