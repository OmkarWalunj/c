/*		1
	  3  2  1
    5  4  3  2  1
          3  2  1
	        1
*/

#include <stdio.h>

void main(){
	int rows;
	printf("Enter rows:");
	scanf("%d",&rows);
	int sp,cols,num;
	for (int i=1;i<2*rows;i++){
		if(i<=rows){
			num=2*i-1;
			sp=2*rows-2*i;
			cols=2*i-1;
		}else{
			//num=3*rows-2*i+rows-1;
			sp=2*i-2*rows;
			cols=cols-2;
			num=cols;
		}
		for(int space=1;space<=sp;space++){
			printf(" \t");
		}
		for(int j=1;j<=cols;j++){
			printf("%d\t",num);
			num--;
		}
		printf("\n");
	}
}
