// macros

#include <stdio.h>
#define xyz 10
#define add(a,b) a+b
#define sqt(x) x*x

void main(){

	printf("%d\n",xyz);   //xyz replace by  10 in preprosessing time
	
	int x=10;
	int y=20;

	printf("%d\n",add(x,y));  //30
	printf("%d\n",sqt(x));   //100
}
