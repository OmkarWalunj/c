#include <stdio.h>

struct movie{
	char mName[20];
	int count;
	float price;
};

void main(){

	//structure pointer

	struct movie obj1={"kantara",2,300.5};
	struct movie * ptr= &obj1;

	printf("%s\n",(*ptr).mName);
	printf("%d\n",(*ptr).count);
	printf("%f\n",ptr->price);
}
