#include <stdio.h>
#include <string.h>

struct movie{
	char mName[20];
	struct movieinfo{
	        char actor[20];
        	float imdb;
	}obj1;
};

void main(){

	//Nested structure 
	//type 2

	struct movie obj={"kantara",{"Rishab",9.5}};
	printf("%ld\n",sizeof(obj));
	printf("%s\n",obj.mName);
	printf("%s\n",obj.obj1.actor);
	printf("%f\n",obj.obj1.imdb);

	struct movie obj2;

	strcpy(obj2.mName,"Tumbbed");
	strcpy(obj2.obj1.actor,"Rishab");
	obj2.obj1.imdb=9.5;

	printf("%s\n",obj2.mName);
        printf("%s\n",obj2.obj1.actor);
        printf("%f\n",obj2.obj1.imdb);

}
