#include <stdio.h>

struct cricplayer{
	char pName[20];
	int jerno;
	float revenue;
};

void main(){

	//structure pointer

	struct cricplayer obj1={"Rohit sharma",45,500.5};
	struct cricplayer obj2={"Virat kholi",18,78.52};
	struct cricplayer obj3={"MsDhoni",7,600.52};
	
	// Array of structure
	struct cricplayer arr[3]={obj1,obj2,obj3};
	for ( int i=0;i<3;i++){
		printf("%s\n",(arr[i]).pName);
		printf("%d\n",arr[i].jerno);
		printf("%f\n",arr[i].revenue);

	}
}
