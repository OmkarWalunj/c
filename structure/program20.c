#include <stdio.h>

struct Demo{

	int x:4;
	int y;

}obj;

void main(){

	printf("%p\n",&obj.x);     //error cannot take address of bit-field ‘x’
	printf("%p\n",&obj.y);    
}
