// typedef

#include <stdio.h>

struct Employee{
	int empid;
	char eName[20];
	float sal;
};
void main(){
	Employee obj={1450,"Omkar",35.50};    //error: unknown type name ‘Employee’; use ‘struct’ keyword to refer to the type

}
