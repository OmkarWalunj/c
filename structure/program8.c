#include <stdio.h>

struct OTT{
	char pName[20];
	int accounts;
	float price;
};

void main(){
	struct OTT obj1={"Netflix",2,460.50};

	struct OTT obj2;
	printf("Enter platform: ");
	gets(obj2.pName);
	printf("Enter No of accounts :");
	scanf("%d",&(obj2.accounts));
	printf("Enter price of subscription:");
	scanf("%f",&(obj2.price));

	printf("%s\n",obj2.pName);
	printf("%d\n",obj2.accounts);
	printf("%f\n",obj2.price);
}
