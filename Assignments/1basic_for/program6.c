/*6. Write a program to print reverse from 100-1
*/
# include <stdio.h>

void main(){
	printf(" 100-1 numbers\n");
	for (int i=100; i>=1; i--){
		printf("%d",i);
	}
}
