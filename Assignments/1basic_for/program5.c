/*5. Write a program to print ASCII values from 0 to 127
*/
# include <stdio.h>

void main(){
	printf("ASCII values:\n");
	for (int i=0; i<=127; i++){
		printf(" %c=%d\n",i,i);
	}
}
