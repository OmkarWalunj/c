/* 2. Write a program to print the first 100 numbers
*/
# include <stdio.h>

void main(){
	printf("First 100 numbers\n");
	for (int i=1; i<=100; i++){
		printf("%d\n",i);
	}
}
