/*9. Write a program to print the sum of the first 10 odd numbers
*/
# include <stdio.h>

void main(){
	int sum=0;
	for (int i=1; i<=10; i++){
		if (i%2 != 0){
			sum=sum+i;
		}
	}
	printf("sum of First 10 odd numbers is :%d\n",sum);
	
}
