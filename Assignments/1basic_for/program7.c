/* 7. Write a program to print a table of 12
*/
# include <stdio.h>

void main(){
	printf("Table of 12:\n");
	for (int i=1; i<=10; i++){
		printf("12*%d=%d\n",i,12*i);
	}
}
