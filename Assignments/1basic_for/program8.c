/* 8. Write a program to print a table of 11 in reverse order
*/
# include <stdio.h>

void main(){
	printf("reverse table of 11:\n");
	for (int i=10; i>=1; i--){
		printf("11*%d=%d\n",i,11*i);
	}
}
