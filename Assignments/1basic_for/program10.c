/*10.Write a program to print the product of the first 10 numbers
*/
# include <stdio.h>

void main(){
	int product=1;
	for (int i=1; i<=10; i++){
		product= product*i;
	}
	printf("product of First 10 numbers is :%d\n",product);
	
}
