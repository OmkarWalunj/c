//
// find out pair of whose sum is 6
#include <stdio.h>

int paircount(int * arr,int sum,int arrsize){
	int count=0;

	for(int i=0;i<arrsize/2;i++){
		for(int j=0;j<arrsize;j++){
			if(arr[i]+arr[j]==sum){
				count++;
			}
		}
	}
	return count;
}

void main(){
	int arr[]={0,1,2,3,4,5,6};
	int arrsize= sizeof(arr)/sizeof(int);

	int sum = 6;
	printf("paircount is %d\n",paircount(arr,sum,arrsize));
}

