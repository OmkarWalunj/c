/* WAP ,that if users give 5 print five if number not found print entered number is grater than five  (using switch case)
 * */
# include <stdio.h>

void main(){
	int no;

	printf("Enter number:\n");
	scanf("%d",&no);

	switch(no){
		case 0:
			printf("0=Zero\n");
			break;
		case 1:
			printf("1=One\n");
			break;
		case 2:
			printf("2=Two\n");
			break;
		case 3:
			printf("3=Three\n");
			break;
		case 4:
			printf("4= Four\n");
			break;
		case 5:
			printf("5= Five\n");
			break;
		default :
			printf(" Entered no is greater than five\n");
			break;
	}
}
		
