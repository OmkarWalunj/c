/* WAP to check whether the given input is a pythagorean triplets or not. */

# include<stdio.h>

void main(){
	int side1 ,side2,side3;

	printf("Enter side1:\n");
	scanf("%d",&side1);

	printf("Enter side2:\n");
	scanf("%d",&side2);

	printf("Enter side3:\n");
	scanf("%d",&side3);
	if (side1 >0 && side2>0 && side3>0){
		if ( side1*side1 + side2*side2 == side3*side3){
			printf("It is triplet of pythagorian\n");
		}else if ( side2*side2 + side3*side3 == side1*side1){
			printf("It is triplet of pythagorian\n");
		}else if ( side3*side3 + side1*side1 == side2*side2){
			printf("It is triplet of pythagorian\n");
		}else{
			printf("It is not of pythagorian\n");
		}
	}else{
		printf("It is not triplet of pythagorian\n");
	}
}

