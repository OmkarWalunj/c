/* WAP to take check whether the input is a leap year or not 
 * input=2000
 * output=leap year
 *
 * input=1999
 * Output=not a leap year
 */

# include <stdio.h>

void main(){
	int input;

	printf("Enter Year:\n");
	scanf("%d",&input);

	if(input%4==0 ){
		printf("Leap year\n");
	}else if( input % 4 !=0){
	       printf("Not a leap year\n");
	}else{
		printf("Enter valid input\n");
	}
}	
