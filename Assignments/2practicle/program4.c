/* WAP ,in which according to the month number print the no.of days in that month (using switch case)
 * */
# include <stdio.h>

void main(){
	int monthno;

	printf("Enter month number:\n");
	scanf("%d",&monthno);

	switch(monthno){
		case 1:
			printf("January has 31 days\n");
			break;
		case 2:
			printf("February has 28/29 days\n");
			break;
		case 3:
			printf("March has 31 days\n");
			break;
		case 4:
			printf("April has 30 days\n");
			break;
		case 5:
			printf("May has 31 days\n");
			break;

		case 6:
			printf("june has 30 days\n");
			break;

		case 7:
			printf("july has 31 days\n");
			break;

		case 8:
			printf("August has 31 days\n");
			break;
		
		case 9:
			printf("September has 30 days\n");
			break;

		case 10:
			printf("Octomber has 31 days\n");
			break;

		case 11:
			printf("November has 30 days\n");
			break;

		case 12:
			printf("December has 31 days\n");
			break;
		default :
			printf("month no not found\n");
			break;
	}
}
		
