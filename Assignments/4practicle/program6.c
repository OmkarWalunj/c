/*Program 6 :
Take an input number from the user and count the no of digits.
Input = 2534
Output = Number of Digits in 2534 is 4*/

# include <stdio.h>

void main(){

	int input,rem,temp, count=0;

	printf("Enter input number:\n");
	scanf("%d",&input);
	temp = input;
	while (temp != 0){

		rem=temp%10;

		temp=temp/10;
		count += 1;
	}
	printf("count of digit of  %d is :%d\n",input,count);
}



