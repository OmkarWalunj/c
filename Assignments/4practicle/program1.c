/*Program 1 :
WAP to Find the sum of numbers that are not divisible by 3 up to a given number
Input: 10
Output: sum of numbers not divisible by 3 is 37
*/

# include<stdio.h>

void main(){

	int input,sum=0;

	printf("Enter input:\n");
	scanf("%d",&input);

	for (int i=1; i<= input;i++){
		if (i%3 !=0){
			sum = sum +i;
		}
	}
	printf("Sum of numbers not divisible by 3 is %d\n",sum);
}
