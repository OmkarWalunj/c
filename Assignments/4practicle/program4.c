/* Program 4:
Write a program to take a number as input and print whether that is a prime
number or not.
{Note: Prime number is the one which is divisible by 1 and that
number only}
Input: 41
Output: 41 is the prime number!
*/

# include <stdio.h>

void main(){
	int input,count=0;

	printf("Enter number:\n");
	scanf("%d",&input);

	for (int i=2; i<= input; i++){
		if (input%i==0){
			count++;
		}
	}
	if (count==1){
		printf("%d is prime number\n",input);
	}else if ( input==1 || input == 0){
		printf("%d is not prime number not even\n",input);
	}else{
		printf("%d is not prime number\n",input);
	}
}
