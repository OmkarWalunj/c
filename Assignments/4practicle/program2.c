/* Program 2:
Write a program to print the addition of 1 to 10 with 10 to 1.
Output:
1 + 10 = 11
2 + 9 = 11
3 + 8 = 11
.
.
10+ 1 = 11*/

# include <stdio.h>

void main(){

	int a= 1,sum=0;

	for(int i=10; i>=1;i--){
		sum = a + i;
		printf("%d  +%d  = %d \n",a,i,sum);
		a++;
	}
}
