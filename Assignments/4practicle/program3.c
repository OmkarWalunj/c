/*Program 3 :
WAP to print the divisors & count of divisors of the entered num.
Input: 15
Output: the divisors are 1 3 5 15
The count of divisors is 4
Additions of divisors 24*/

# include<stdio.h>

void main(){
	int num,count=0,add=0;

	printf("Enter number:\n");
	scanf("%d",&num);

	for (int i=1;i<=num;i++){
		if (num%i==0){
			printf("The divisor are %d\n",i);
			count++;
			add =add + i;
		}
	}
	printf("count of divisor is :%d\n",count);
	printf("Addition of divisor is :%d\n",add);
}
