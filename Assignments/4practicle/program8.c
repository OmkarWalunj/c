/*Program 8: Take input from the user and print the product of digits
Input = 134
Output = product of digits is 12 */

# include<stdio.h>

void main(){

	int input,rem,prod=1,temp;
	printf("Enter input:\n");
	scanf("%d",&input);
	temp = input;
	while (temp >=1){
		rem= temp% 10;
		prod = prod*rem;
		temp= temp/10;
	}
	printf("Product of digits is %d\n",prod);
}
		


