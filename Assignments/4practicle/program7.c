/*Program 7 :
Take an input number from the user and print the sum of digits.
Input = 1234
Output = sum of digits is 10**/

# include <stdio.h>

void main(){

	int input,rem,temp,sum=0;

	printf("Enter input number:\n");
	scanf("%d",&input);
	temp = input;
	while (temp != 0){

		rem=temp%10;
		sum =sum + rem;
		temp=temp/10;
	
	}
	printf("sum of digit of  %d is :%d\n",input,sum);
}



