/*Program 10 :
Take a number from the user and print the Fibonacci series up to that number.
Input : 10
Output 0 1 1 2 3 5 8*/

# include <stdio.h>

void main(){

  	int input,num1 = 1, num2 = 1, sum = 0;
	printf("Enter a input: ");
  	scanf("%d", &input);

  	while (sum < input) {
    		printf("%d, ", sum);
    		num1 = num2;
    		num2= sum;
    		sum = num1 + num2;

	}
	printf("\n");
}
