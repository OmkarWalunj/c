/*Program 5:
WAP to print all even numbers in reverse order and odd numbers in the standard way.
Both separately. Within a range.
Input: start - 2
End - 9
Output:
8 6 4 2
3 5 7 9*/

# include <stdio.h>

void main(){

	int start,end,odd;

	printf("Enter start number:\n");
	scanf("%d",&start);

	printf("Enter end number:\n");
	scanf("%d",&end);
	for (int i =end ;i >= start; i--){
		if(i % 2 ==0){
			printf("%d ",i);
		}
	}
	printf("\n");
	for (int i= start ; i<= end; i++){
		if(i%2 !=0){ 
			printf("%d ",i);
		
		}
	}
	printf("\n");
}

