/*3. WAP to print all even numbers in reverse order and odd numbers in the standard
way. Both separately. Within a range.*/

#include <stdio.h>

void main(){

	int start,end;

	printf("Enter starting number:\n");
	scanf("%d",&start);
	
	printf("Enter ending number:\n");
	scanf("%d",&end);
	
	printf("Odd numbers: ");
	for (int i=start; i<=end;i++){
		if ( i% 2!=0){
			printf("%d  ",i);
		}
	}
	printf("\n");
	
	printf("Even numbers: ");
	for (int i=end; i>= start; i--){
		if (i % 2==0){
			printf("%d  ",i);
		}
	}
	printf("\n");
}

