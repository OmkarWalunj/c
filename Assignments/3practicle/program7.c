/*7. WAP to calculate the LCM of given two numbers.*/

#include <stdio.h>

void main(){

	int num1,num2,LCM,gcd;

	printf("Enter num1 :\n");
	scanf("%d",&num1);
	
	printf("Enter num2 :\n");
	scanf("%d",&num2);

	for (int i=1 ; i<=num1 && i<=num2; i++){

		if (num1%i==0 && num2 %i==0){
			gcd= i;
		}
	}
	LCM= (num1*num2)/gcd;
	printf("The LCM of two numbers %d and %d is %d\n",num1,num2,LCM);

}

