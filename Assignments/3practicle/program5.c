/*5. WAP to take the Number input and print all the factors of that number.*/

# include<stdio.h>

void main(){
	int num;

	printf("Enter number:\n");
	scanf("%d",&num);

	printf("factors of %d is :",num);
	for (int i=1;i<=num;i++){
		if (num%i==0){
			printf(" %d ",i);
		}
	}
	printf("\n");
}
