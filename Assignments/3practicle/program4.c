/*4. WAP to Find the sum of numbers that are divisible by 5 in the given range */

#include <stdio.h>

void main(){

	int start,end,sum =0;

	printf("Enter starting number:\n");
	scanf("%d",&start);
	
	printf("Enter ending number:\n");
	scanf("%d",&end);

	for (int i=start; i<=end;i++){
		if ( i% 5==0){
			sum = sum + i;
		}
	}
	printf("sum of numbers that are divisible by 5 is :%d\n",sum);
	
}

