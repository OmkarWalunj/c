/*10. WAP to print the numbers in a given range and their multiplicative inverse.
Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
The expected output for range 1 - 5
1 = 1
2 = 1/2 i.e 0.5
3 = 1/3 i.e 0.33
4 = 0.25
5 = 0.20
*/

# include <stdio.h>

void main(){
	int start,end;
	float inv;
	printf("Enter start number:\n");
	scanf("%d",&start);
	
	printf("Enter end number:\n");
	scanf("%d",&end);

	for (int  i=start; i<= end; i++){
		inv=1.0/i;
		printf("%d = %f\n",i,inv);
	}
}


