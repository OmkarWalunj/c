/*9. WAP to calculate the square root of a number ranging from 100 to 300*/

# include <stdio.h>

void main(){

	float sqr,temp;
	for (int i=100; i<=300; i++){
		sqr=i/2;
		temp=0;
		while(sqr!=temp){
			temp=sqr;
			sqr=(i/temp+temp)/2;
		}
		printf("Square root of %d is %f\n",i,sqr);
	}
}
