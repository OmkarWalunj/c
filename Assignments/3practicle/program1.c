/*1. WAP a program to see a given number is a multiple of 3
 * */
#include<stdio.h>

void main(){
	int val;

	printf("Enter number:\n");
	scanf("%d",&val);

	if (val % 3==0){
		printf("Number is multiple of 3\n");
	}else{
		printf("Number not multiple of 3\n");
	};
}

