/*6. take no of rows from the user
 
  =  =  =  =  =  =
  $  $  $  $  $  $
  @  @  @  @  @  @
  =  =  =  =  =  =
  $  $  $  $  $  $
  @  @  @  @  @  @
*/

# include<stdio.h>

void main(){
	int rows;
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	for (int i =1;i<=rows;i++){
		for(int j=1;j<=rows;j++){
			if (i%3==1){
				printf("=\t");
			}else if(i%3==0){
				printf("@\t");
			}else {
				printf("$\t");
			}
		}
		printf("\n");
	}
}
