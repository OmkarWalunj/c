/* 9. take no of rows from the user
 
   0   1   1   2
   3   5   8   13
   21  34  55  89
   144 233 377 610
  */
#include<stdio.h>

void main(){

	int num1=1,num2=1,num3=0,rows;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i =1;i<=rows;i++){
		for(int j=1;j<=rows;j++){
			printf("%d\t",num3);

			num1=num2;
			num2=num3;
			num3=num1+num2;
		}
		printf("\n");
	}
}
