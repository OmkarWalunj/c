/* 8.take no of rows from user
   
   16  15  14  13
   L   K   J   I
   8   7   6   5
   D   C   B   A
*/
#include<stdio.h>

void main(){
	int num,ch,rows;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	num=rows*rows;
	ch=65+num-1;
	for (int i=1;i<=rows;i++){
		for(int j=1;j<=rows;j++){
			if(i%2 !=0){
				printf("%d\t",num);
			}else{
				printf("%c\t",ch);
			}
			ch--;
			num--;
		}
		printf("\n");
	}
}
