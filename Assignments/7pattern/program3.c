/*3. take no of rows from the user
  
  4  a  3  b
  4  a  3  b
  4  a  3  b
  4  a  3  b
  
 */
#include<stdio.h>

void main(){

	int ch,rows,num;
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++){
		ch=97;
		num=rows;
		for(int j=0;j<rows;j++){
				if (j%2==0){
					printf("%d\t",num);
					num--;
				}else{
					printf("%c\t",ch);
					ch++;
				}
		}
		printf("\n");
	}
}
