/*1. take no of rows from the user
   
  1  2  3  4
  1  3  5  7
  1  4  7  10
  1  5  9  13

  */
# include <stdio.h>

void main(){

	int num1,rows;
	printf("Enter no of rows:\n");
        scanf("%d",&rows);	
	for (int i=1;i<=rows;i++){
		num1=1;
		for (int j=1;j<=rows;j++){
			printf("%d\t",num1);
			num1 +=i;
		}
		printf("\n");
	}
}

