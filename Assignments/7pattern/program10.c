/* 10.take no rows from user
 
   D1  C2  B3  A4
   e5  f4  g3  h2
   F3  E4  D5  C6
   g7  h6  i5  j4
*/
#include<stdio.h>

void main(){

	int num1,num2,rows,ch;
	
	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for(int i=0;i<rows;i++){
		num1=1+i;
		num2=rows+i;
		ch=64+rows+i;
		for(int j=1;j<=rows;j++){
			if(i%2==0){
				printf("%c%d\t",ch,num1);
				ch--;
				num1++;
			}else{
				printf("%c%d\t",ch+32,num2);
				ch++;
				num2--;
			}
			
		}
		printf("\n");
	}
}
