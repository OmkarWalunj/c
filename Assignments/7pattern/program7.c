/* 2. take no of rows from the user
 
   1  4  27
   4  27 16
   27 16 125
 */
#include<stdio.h>

void main(){
	int num,rows;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for(int i=0;i<rows;i++){
		num=1+i;
		for (int j=1;j<=rows;j++){
			if(num % 2 !=0){
				printf("%d\t",num*num*num);
			}else{
				printf("%d\t",num*num);
			}
			num++;
		}
		printf("\n");
	}
}
