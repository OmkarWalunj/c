/*4. take no of rows from the user
  
  a  B  c  D
  b  C  d  E
  c  D  e  F
  d  E  f  G
 */
#include<stdio.h>

void main(){

	int ch,rows;
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	for(int i=0;i<rows;i++){
		ch=97+i;
		for(int j=1;j<=rows;j++){
				if (j%2!=0){
					printf("%c\t",ch);
				}else{
					printf("%c\t",ch-32);
				}
				ch++;
			}
		printf("\n");
	}
}
