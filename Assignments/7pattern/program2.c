/*2. take no of rows from the user
  
  3  b  1  d
  a  2  c  0
  3  b  1  d
  a  2  c  0
 */
#include<stdio.h>

void main(){

	int ch,rows;
	printf("Enter no of rows:\n");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++){
		ch=97;
		for(int j=1;j<=rows;j++){
			if(i%2 !=0){
				if (j%2!=0){
					printf("%d\t",rows-j);
				}else{
					printf("%c\t",ch);
				}
				ch++;
			}else{
				if(j%2!=0){
					printf("%c\t",ch);
				}else{
					printf("%d\t",rows-j);
				}
			}
		}
		printf("\n");
	}
}
