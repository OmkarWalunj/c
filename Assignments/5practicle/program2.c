/* 2. If possible take no of rows from the user
    1  2  3    
    a  b  c  
    1  2  3  
    a  b  c
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=97;
		for (int j=1;j<=cols;j++){
			if (i% 2 == 0){
				printf("%d\t",j);
			}else{
				printf("%c\t",num);
				num++;
			}
		}
		printf("\n");

	}
}

