/* 3. If possible take no of rows from the user
    1  1  1  1
    2  2  2  2
    3  3  3  3
    4  4  4  4
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=i+1;
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
		}
		printf("\n");

	}
}

