/* 9. If possible take no of rows from the user    
    2  5  10  
    17 26 37  
    50 65 82
*/

# include<stdio.h>

void main(){
	int rows,cols,num=1,sum=1;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			sum=sum+num;
			printf("%d\t",sum);
			num=num+2;
		}
		printf("\n");

	}
}

