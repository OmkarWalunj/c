/* 7. If possible take no of rows from the user
    1    2   9    4    
    25   6   49   8
    81   10  121  12
    169  14  225  16
*/

# include<stdio.h>

void main(){
	int rows,cols,num=1;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			if (j% 2 != 0){
				printf("%d\t",num*num);
			}else{
				printf("%d\t",num);
			}
			num++;
		}
		printf("\n");

	}
}

