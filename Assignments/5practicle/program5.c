/* 2. If possible take no of rows from the user    
    A  B  C  D
    B  C  D  E
    C  D  E  F
    D  E  F  G
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=65+i;
		for (int j=1;j<=cols;j++){
			printf("%c\t",num);
			num++;
		}
		printf("\n");

	}
}

