/* 10. If possible take no of rows from the user    
    D4  C3  B2  A1
    D4  C3  B2  A1
    D4  C3  B2  A1
    D4  C3  B2  A1
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=64+rows;
		for (int j=cols;j>=1;j--){
			printf("%C%d\t",num,j);
			num--;
		}
		printf("\n");

	}
}

