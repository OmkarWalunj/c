/* 8. If possible take no of rows from the user    
    1  3  5  
    7  9  11  
    13 15 17
*/

# include<stdio.h>

void main(){
	int rows,cols,num=1;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num=num+2;
		}
		printf("\n");

	}
}

