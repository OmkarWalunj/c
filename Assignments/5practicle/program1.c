/* 1. If possible take no of rows from the user
    1  2  3  4  
    2  3  4  5
    3  4  5  6
    4  5  6  7
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=i+1;
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");

	}
}

