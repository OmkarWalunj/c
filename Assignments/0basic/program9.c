/*
Program 9: Write a program, take a number and print whether it is positive or
negative. Take all the values from the user
Input: var=5
Output: 5 is a positive number
Input: var=-9
Output: -9 is a negative number
*/

#include <stdio.h>

void main(){

	float  var;

	printf("Enter number:\n");
	scanf("%f",&var);

	if (var>0.0){
		printf("%f is positive number\n",var);
	}else{
		printf("%f is negative number\n",var);
	}
}
