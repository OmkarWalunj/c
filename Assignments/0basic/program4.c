/*
Program 4: WAP to find min among 2 numbers. Take all the values from the user
Input : 2 3
Output: 2
*/

#include <stdio.h>

void main(){

	int  x;
	int  y;

	printf("Enter number x:\n");
	scanf("%d",&x);

	printf("Enter number y:\n");
	scanf("%d",&y);

	if (x<y){
		printf("Min %d and %d is :%d\n",x,y,x);
	}else if (x==y){
		printf("both same\n");
	}else{
		printf("Min %d and %d is :%d\n",x,y,y);
	}
}
