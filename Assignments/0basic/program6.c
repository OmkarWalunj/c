/*
Program 6:
Write a program to check if a number is even or odd. Take all the values
from the user
Input: var=10;
Output: 10 is an even no
Input: var=37;
Output: 37 is an odd no
*/

#include <stdio.h>

void main(){

	int  input;

	printf("Enter input:\n");
	scanf("%d",&input);

	if (input & 1 == 1){
		printf("%d is a odd number\n",input);
	}else{
		printf("%d is a even number\n",input);
	}
}
