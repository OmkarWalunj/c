/*Program 1:
Write a program to print the value and size of the below variables.Take all
the values from the user
num=10
chr = ‘S’
rs = 55.20
crMoney = 542154313480.544543
*/

#include <stdio.h>

void main(){
	int  num;
	char  ch;
	float rs;
	double crMoney;

	printf("Enter number: \n");
	scanf("%d",&num);

	printf("Enter character:\n");
	scanf(" %c",&ch);
	
	printf("Enter Float value:\n");
	scanf("%f",&rs);
	
	printf("Enter Double value:\n");
	scanf("%le",&crMoney);

	printf("Size of number is :%ld\n",sizeof(num));
	printf("Size of character is :%ld\n",sizeof(ch));
	printf("Size of Float number is :%ld\n",sizeof(rs));
	printf("Size of Double number is :%ld\n",sizeof(crMoney));
}
	

