/*
Program 7:
Write a program, take a number and print whether it is less than 10 or greaterthan 10. Take all the values from the user
Input: var=5
Output: 5 Is Less than 10.
Input: var=16
Output: 16 Is greater than 10.Output: 37 is an odd no
*/

#include <stdio.h>

void main(){

	int  input;

	printf("Enter input:\n");
	scanf("%d",&input);

	if (input < 10){
		printf("%d is a less than 10\n",input);
	}else{
		printf("%d is a greater than 10\n",input);
	}
}
