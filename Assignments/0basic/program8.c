/*
Program 8: Write a program, take a character and print whether it is in
UPPERCASE or lowercase. Take all the values from the user
Input: var = A
Output: You entered the UPPERCASE character.
*/

#include <stdio.h>

void main(){

	char  ch;

	printf("Enter character:\n");
	scanf("%c",&ch);

	if (ch >= 'A' && ch <= 'Z'){
		printf("You Entered the UPPERCASE character\n");
	}else if (ch >='a' && ch <= 'z'){
		printf("YOu ENtered the lowercase character\n");
	}else{
		printf("please enter character only\n");
	}
}
