// Differce between maximum even number and minimum odd no 

#include <stdio.h>

int fun(int arr[],int size){
	int max=-2147483648;
	int min=2147483647;
	for(int i=0;i<size;i++){
		if(arr[i] >max){
			if(arr[i] % 2==0){
				max=arr[i];
			}
		}
		if(arr[i] < min){
			if(arr[i] % 2 != 0){
				min=arr[i];
			}
		}
	}
	return max-min;
}

void main(){

	int size;
	printf("Enter Array size :\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array elements:\n");

	for(int i=0;i<size;i++){
	       scanf("%d",&arr[i]);	
	} 
	printf("Differce is %d\n",fun(arr,size));
}
