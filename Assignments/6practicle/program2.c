/*
 *   3  2  1
 *   c  b  a
     3  2  1
     c  b  a
*/

# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=99;
		for (int j=cols;j>=1;j--){
			if (i% 2 == 0){
				printf("%d\t",j);
			}else{
				printf("%c\t",num);
				num--;
			}
		}
		printf("\n");

	}
}

