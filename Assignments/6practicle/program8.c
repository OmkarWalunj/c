/*  prgram 8:
 
    18  16  14
    12  10  8
    6   4   2
*/
# include<stdio.h>

void main(){
	int rows,cols;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	int num=rows*cols*2;
	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num -=2;
		}
		printf("\n");

	}
}
