/*
   4 3 2 1
   5 4 3 2
   6 5 4 3
   7 6 5 4 
  */
# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=rows+i;
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			num--;
		}
		printf("\n");

	}
}

