/*
 
   I  H  G
   F  E  D
   C  B  A
*/
# include<stdio.h>

void main(){
	int rows,cols;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);
	
	int num=64+rows*cols;
	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			printf("%c\t",num);
			num--;
		}
		printf("\n");

	}
}
