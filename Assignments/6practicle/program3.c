/*
 
   4  4  4  4
   3  3  3  3
   2  2  2  2
   1  1  1  1
  */
# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=rows-i;
		for (int j=1;j<=cols;j++){
			printf("%d\t",num);
			
		}
		printf("\n");

	}
}

