/*/* 6. If possible take no of rows from the user
    =  =  =  =    
    $  $  $  $
    =  =  =  =
    $  $  $  $
*/

# include<stdio.h>

void main(){
	int rows,cols;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);
	
	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		for (int j=1;j<=cols;j++){
			if (i% 2 == 0){
				printf("=\t");
			}else{
				printf("$\t");
			}
		}
		printf("\n");

	}
}

