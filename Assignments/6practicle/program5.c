/*  program 5:
    
    D  C  B  A
    e  d  c  b
    F  E  D  C
    g  f  e  d
*/
# include<stdio.h>

void main(){
	int rows,cols,num;

	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	printf("Enter number of cols:\n");
	scanf("%d",&cols);

	for (int i= 0;i<rows;i++){
		num=64+rows+i;
		for (int j=1;j<=cols;j++){
			if (i%2 == 0){
				printf("%c\t",num);
			}else{
				printf("%c\t",num+32);
			}
			num--;
		}
		printf("\n");

	}
}
