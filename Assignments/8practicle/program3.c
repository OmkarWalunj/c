/* WAP to print composite number from given range.*/

#include <stdio.h>

void main(){

	int start ,end,count;

	printf("Enter start no :\n");
	scanf("%d",&start);
	
	printf("Enter end no :\n");
	scanf("%d",&end);

	for (int i=start;i<=end;i++){
		count=0;
		for (int j=2;j<=i;j++){

		        if (i%j ==0){
				count++;
			}
		}
		if(count >= 2){
			printf("%d ",i);
		}
	
		
	}
	printf("\n");
}

