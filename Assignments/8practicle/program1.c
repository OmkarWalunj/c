/* 1.WAP To print all the divisor of the given no
   input:30
   output:1,2,3,5,6,10,15,30

*/

#include<stdio.h>

void main(){

	int num;

	printf("Enter number :\n");
	scanf("%d",&num);

	for (int i=1;i<=num;i++){
		if(num%i==0){
			printf("%d ",i);
		}
	}
	printf("\n");
}
