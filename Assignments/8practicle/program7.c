/*
   
   E  D  C  B  A
   E  D  C  B
   E  D  C
   E  D
   E
 */

#include<stdio.h>

void main(){

	int rows,ch;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=0;i<rows;i++){
		ch=64+rows;
		for (int j=0;j<rows-i;j++){
			printf("%c\t",ch);
			ch--;
		}
		printf("\n");
	}
}
