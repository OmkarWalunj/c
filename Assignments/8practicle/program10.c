/*
 
   D  D  D  D
      C  C  C
         B  B
	    A
*/

#include<stdio.h>

void main(){

	int rows,ch;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=0;i<rows;i++){
		ch=64+rows-i;
		for (int j=0;j<i;j++){
			printf(" \t");
		}
		for (int k=0;k<rows-i;k++){
			printf("%c\t",ch);
		}
		printf("\n");
	}
}
