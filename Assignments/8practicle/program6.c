/*
  
   D  e  F  g
   e  D  c  B
   F  g  H  i
   g  F  e  D

  */

#include<stdio.h>
void main(){

	int rows,ch;

	printf("Enter no of rows:\n");
	scanf("%d",&rows);

	for (int i=0;i<rows;i++){
		ch=64+rows+i;
		for (int j=0;j< rows;j++){
			if(i%2==0){
				if(j%2==0){
					printf("%c\t",ch);
				}else{
					printf("%c\t",ch+32);
				}
				ch++;
			}else{
				if(j %2 !=0){
					printf("%c\t",ch);
				}else{
					printf("%c\t",ch+32);
				}
				ch--;
			}
		}
		printf("\n");
	}
}

