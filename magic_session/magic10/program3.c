/* WAP that dynamically allocate memmory a 3-D array of marks ,take value from user and print it .[use malloc]*/

#include <stdio.h>
#include <stdlib.h>

void main(){

	int rows,cols ,plane;
	printf("Enter no of rows :\n");
	scanf("%d",&rows);
	
	printf("Enter no of cols :\n");
	scanf("%d",&cols);
	
	printf("Enter no of plane :\n");
	scanf("%d",&plane);

	int *** ptr = (int ***)malloc(plane*sizeof(int**));
	for(int i=0;i<plane;i++){
		*(ptr+i) =(int **)malloc (rows*sizeof(int*));
		for (int j=0;j<rows;j++){
			*(*(ptr+i)+j)=(int*)malloc(cols*sizeof(int));
		}
	}
	
	printf("Enter Array Elements :");
	for (int i=0;i<rows*cols*plane;i++){
		scanf("%d ",(*(*(ptr))+i));
	}
	printf("Array elements are :");
	for (int i=0;i<rows*cols*plane;i++){
		printf("%d  ",*(*(*(ptr))+i));
	}
	printf("\n");

	for (int i=0;i<plane ;i++){
		for ( int j=0;j<rows;j++){
			free(ptr[i][j]);
		}
		free(ptr[i]);
	}
	free(ptr);
}
