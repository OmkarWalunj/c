/* WAP that dynamically allocate memmory a 1-D array of marks ,take value from user and print it .[use malloc]*/

#include <stdio.h>
#include <stdlib.h>

void main(){

	int size ;
	printf("Enter array size :\n");
	scanf("%d",&size);

	int * ptr = (int *)malloc (size*sizeof(int));
	printf("Enter Array Element\n");
	for (int i=0;i<size;i++){
		scanf("%d",(ptr+i));
	}
	printf("Array Elements are:");
	for (int i=0;i<size;i++){
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}
