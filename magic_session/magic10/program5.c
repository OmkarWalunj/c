#include <stdio.h>
#include<stdlib.h>

void main(){

	int rows=4;
	int cols=3;

	int ** ptr = (int**)malloc(rows*sizeof(int*));

	for (int i=0;i<rows;i++){
		*(ptr+i)=(int*)malloc(cols*sizeof(int));
	}

	for ( int i=0;i<rows;i++){
		free(ptr[i]);
	}
	free(ptr);
}
