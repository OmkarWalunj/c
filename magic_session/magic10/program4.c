/* WAP that dynamically allocate memmory a 2-D array of marks ,take value from user and print it .[use malloc]*/

#include <stdio.h>
#include <stdlib.h>

void main(){

	int rows,cols;
	printf("Enter no of rows :\n");
	scanf("%d",&rows);
	
	printf("Enter no of cols :\n");
	scanf("%d",&cols);

	int ** ptr = (int **)malloc(rows*sizeof(int*));
		for (int j=0;j<rows;j++){
			*(ptr+j)=(int*)malloc(cols*sizeof(int));
		}
	
	printf("Enter Array Elements :");
	for (int i=0;i<rows*cols;i++){
		scanf("%d",(*(ptr)+i));
	}
	printf("Array elements are :");
	for (int i=0;i<rows*cols;i++){
		printf("%d  ",*(*(ptr)+i));
	}
	printf("\n");
	 for ( int i=0;i<rows;i++){
                free(ptr[i]);
        }
        free(ptr);
}
