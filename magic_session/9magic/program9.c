/*Create a 2D array and take the input of elements from the user & Print using a pointer.
Draw a correct diagram and Write the output*/

#include<stdio.h>

void main (){

	int rows,cols;

	printf("Enter no of rows: ");
	scanf("%d",&rows);	
	
	printf("Enter no of cols: ");
	scanf("%d",&cols);	
	
	printf("Enter array elements :\n");
	int arr[rows][cols];
	for (int i=0;i<rows;i++){
		for (int j=0;j<cols;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	for (int i=0;i<rows;i++){
                for (int j=0;j<cols;j++){
                        printf("%d  ",*(*(arr+i)+j));
                }
		printf("\n");
        }
}


