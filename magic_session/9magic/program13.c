/*13. WAP to print different datatype of elements using Void pointer
int , char , float , double*/

#include<stdio.h>

void main(){

	int x=10;
	char ch ='A';
	float F = 20.3;
	double LF =20.02356;

	void *arr[]={&ch,&x,&F,&LF};

	printf("%c\n",*((char*)arr[0]));
	printf("%d\n",*((int*)arr[1]));
	printf("%f\n",*((float*)arr[2]));
	printf("%lf\n",*((double*)arr[3]));

}


