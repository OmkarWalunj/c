/*12.12. WAP to print addition of both diagonal elements using pointer without repeating the
middle element*/

#include<stdio.h>

void main (){

	int rows,cols,sum=0;

	printf("Enter no of rows: ");
	scanf("%d",&rows);	
	
	printf("Enter no of cols: ");
	scanf("%d",&cols);	
	
	printf("Enter array elements :\n");
	int arr[rows][cols];
	for (int i=0;i<rows;i++){
		for (int j=0;j<cols;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	for (int i=0;i<rows;i++){
                for (int j=0;j<cols;j++){
			if (  i+j == 2 || i==j ){
				sum += arr[i][j];
			}
                        printf("%d  ",*(*(arr+i)+j));
                }
		printf("\n");
        }
	printf("Sum of daigonal elements are %d\n",sum);
}


