/*3. WAP to add two different arrays of the same size
Take array size and array elements from the user
IP : enter 1st array : 10 12 13 15
Ip : enter 2nd array: 1 2 3 4
Op: 11 14 16 19
*/

#include <stdio.h>

void main(){
	int size;

	printf("Enter size:\n");
	scanf("%d",&size);

	int iarr1[size];
	int iarr2[size];

	printf("Enter first Array element:\n");
	for (int i=0; i<size;i++){
		scanf("%d",&iarr1[i]);
	}

	printf("Enter second array numbers :\n");
	
	for (int j=0;j<size;j++){
		scanf("%d",&iarr2[j]);
	}
	for (int k=0;k<size;k++){
		printf("%d ",iarr1[k]+iarr2[k]);
	}
	printf("\n");
}


