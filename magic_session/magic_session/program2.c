/*2. WAP to calculate the count of even and odd elements
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 17 19 20 22 23
OP: even element count is
OP: odd element count is
*/

#include <stdio.h>

void main(){
	int size,flag=0,count=0;

	printf("Enter size:\n");
	scanf("%d",&size);

	int iarr[size];
	printf("Enter Array element:\n");
	for (int i=0; i<size;i++){
		scanf("%d",&iarr[i]);
		if (iarr[i] % 2==0){
			flag++;
		}else {
			count++;
		}
	}
	printf("even number count is %d\n",flag);
	printf("odd number count is%d\n",count);
}


