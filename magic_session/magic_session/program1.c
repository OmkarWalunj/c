/*1. WAP to find the given element from the array
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 14
Ip : enter element : 15
Op: 15 is present
*/

#include <stdio.h>

void main(){
	int size,flag=0,num=0;

	printf("Enter size:\n");
	scanf("%d",&size);

	int iarr[size];
	printf("Enter Array element:\n");
	for (int i=0; i<size;i++){
		scanf("%d",&iarr[i]);
	}

	printf("Enter search number :\n");
	scanf("%d",&num);
	
	for (int j=0;j<size;j++){
		if (num==iarr[j]){
			flag=1;
			break;
		}
	}
	if (flag==1){
		printf("%d is found\n",num);
	}else{
		printf("%d is not found\n",num);
	}
}


