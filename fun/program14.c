#include <stdio.h>
int arradd1(int (*arr)[3],int,int);
int arradd2(int (*arr)[3],int);
void main(){
	int arr[3][3] ={10,20,30,40,50,60,70,80,90};

	int arrsize= sizeof(arr)/sizeof(int);
	
	int sum1= arradd1(arr,3,3);
	int sum2 =arradd2(arr,arrsize);
	printf("%d\n",sum1);
	printf("%d\n",sum2);

}

int arradd1(int (*arr)[3],int x,int y){
	int sum =0;
	for (int i=0;i<x;i++){
		for (int j=0;j<y;j++){
			if (i==j){
				sum =sum+*(*(arr+i)+j);
			}
		}
	}
	return sum;
}

int arradd2(int (*arr)[3],int arrsize){
	int sum =0;
	for (int i=0;i<arrsize;i++){
		sum = sum + *(*(arr)+i);
		i = i+3;
	}
	return sum;
}
