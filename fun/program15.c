#include <stdio.h>
int arradd1(int (*arr)[3][3],int,int,int);
int arradd2(int (*arr)[3][3],int,int);
void main(){
	int arr[2][3][3] ={{1,2,3,4,5,6,7,8,9},{10,11,12,13,14,15,16,17,18}};

	int arrsize= sizeof(arr)/sizeof(int);

	int sum1= arradd1(arr,2,3,3);
	int sum2 =arradd2(arr,2,arrsize);
	printf("%d\n",sum1);
	printf("%d\n",sum2);

}

int arradd1(int (*arr)[3][3],int plane,int rows,int cols){
	int sum =0;
	for (int i=0;i<plane;i++){
		for (int j=0;j<rows;j++){
			for (int k=0;k<cols;k++){
				if (j==k || j+k ==rows-1){
					sum =sum+*(*(*(arr+i)+j)+k);
				}
			}
		}
	}
	return sum;
}

int arradd2(int (*arr)[3][3] ,int plane,int arrsize){
	int sum =0;
	for (int i=0;i<arrsize;i++){
		if (i==(arrsize/plane)+1){
			i=i-2;
		}else{
			sum = sum + *(*(*(arr))+i);
			i++;
		}
	}
	return sum;
}
