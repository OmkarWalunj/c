#include <stdio.h>

int * fun(int x,int y){
	printf("%d\n",x+y);       //30
	int val ;
	val = x+y;

	return &val;        //warning : return  local variable add 
}

void main(){

	int *ptr = fun(10,20);

	printf("%p\n" ,ptr);  //nil
	printf("%d\n",*ptr);  //segmentation fault (code dumped)
}
