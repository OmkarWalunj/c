#include <stdio.h>

void add(int x,int y){
	printf("%d\n",x+y);
}

void sub(int x, int y){
	printf("%d\n",x-y);
}
void main(){
	void (*ptr)(int ,int );

	ptr = &add ;             //&add == add
	ptr(10,20);

	ptr = sub;
	ptr (20,10);

}
