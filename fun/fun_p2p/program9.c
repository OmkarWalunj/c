//returning array from function
/*WAP to take an array from the user in another function & print that array in main function take the array size from user*/

#include <stdio.h>

int * fun(int * arr,int arrsize){
	printf("Enter array elements:\n");
	for (int i=0 ; i<arrsize;i++){
		scanf("%d",&arr[i]);
	}

	return arr;
}
void main(){
	int size;
	printf("Enter arr size:");
	scanf("%d",&size);
	int arr[size];
	int * ptr =fun(arr,size);
	printf("Print array elements:\n");
	for (int i =0 ; i<size;i++){	
		printf("%d\n",*(ptr+i));
	}
}

