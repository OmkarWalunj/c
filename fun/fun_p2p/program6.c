#include <stdio.h>


int fun(int a,int b){
	printf("%d\n",a+b);
	return a+b;
}

void main(){

	int (*ptr)(int ,int);
	ptr = fun;
	int sum = ptr(10,20);
	printf("%d\n",sum);
}
