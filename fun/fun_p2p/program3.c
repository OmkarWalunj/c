#include <stdio.h>

void add(int a ,int b){
	printf(" 1 = %d\n",a+b);
	printf(" 2 = %d\n",a+b);
	printf(" 3 = %d\n",a+b);
}

void main(){

	void (*ptr)(int ,int );
	ptr = add;  
	ptr(20,10);
	ptr++;            //increment does'nt matter in function
	ptr(30,40);
}

/*Add is 1st instrustion of address after add in function ptr add increment the pointer  then call ptr  o/p will be same beacuse function stack will push from starting 
 * we can increment only one time*/


