#include <stdio.h>

void main(){
	int  input;
	printf("Enter value: \n");
	scanf("%d",&input);
	int   x=2;
	float y=3;

	switch(input){
		case  5:
			printf("Value of input is 5\n");
			break;
		case 2+3:
			printf("Value of input is 2+3\n"); //Duplicate case error
			break;
		case y+x:                       // case lable does not reduce to an integer 
			printf("Value of x is 3+2\n");
			break;
		default:
			printf("wrong input\n");
	}
}

