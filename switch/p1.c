#include <stdio.h>

void main(){
	int  x;
	printf("Enter value: \n");
	scanf("%d",&x);

	switch(x){
		case  65:
			printf("Value of x is 65\n");
			break;
		case 'A':
			printf("Value of x is A\n"); //Duplicate case error
			break;
		case 'B':
			printf("Value of x is B\n");
			break;
		default:
			printf("wrong input\n");
	}
}

