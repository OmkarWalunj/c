//null pointer and wild pointer


#include <stdio.h>

void main (){

	int * ptr;     //wild pointer

	int *ptr1 = 0;
	int * ptr2 = NULL;   //null pointer
	
	printf("%p\n",ptr);   //nil
	printf("%p\n",ptr1);  //nil 

	printf("%d\n",*ptr);  //core dump
	printf("%d\n",*ptr1);
}
