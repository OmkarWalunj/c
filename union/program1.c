// Union 

#include <stdio.h>

struct Demo{

	int x;
	float y;
	double z;
}obj1;

union demo{

	int x;
	float y;
	double z;
}obj2;

void main(){

	printf("%ld\n",sizeof(obj1));   //16
	printf("%ld\n",sizeof(obj2));   //8

	printf("%p\n",&obj1.x);         //0x100
	printf("%p\n",&obj1.y);		//0x104
	printf("%p\n",&obj1.z);		//0x108
	
	printf("%p\n",&obj2.x);		//0x200
	printf("%p\n",&obj2.y);		//0x200
	printf("%p\n",&obj2.z);		//0x200
}
