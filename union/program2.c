/*   initializer list in union*/

#include <stdio.h>

union Employee{

	int empid;
	float sal;
};

void main(){

	union Employee obj1={10,50.60};	//warning: excess elements in union initializer

	printf("%p\n",&obj1.empid);  //0x100
	printf("%p\n",&obj1.sal);    //0x100

	printf("%d\n",obj1.empid);   //10
	printf("%f\n",obj1.sal);     //0.0000
	printf("%d\n",obj1.sal);     //garbage value
}
