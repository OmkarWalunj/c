// nested union

#include <stdio.h>

union Demo{
	//int z;
	struct demo1{
		int x;
		float y;
	}obj;
}obj1;

void main(){
	union Demo obj2={{10,20.3}};
	printf("%ld\n",sizeof(obj1));
}
