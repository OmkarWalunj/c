//  Proper way for initializes data in union

#include <stdio.h>

union Demo{

	int x;
	float y;
};

void main(){

	union Demo obj;

	obj.x=15;
	printf("%d\n",obj.x);     //15
	obj.y=70.45;
	printf("%f\n",obj.y);     //70.45
}
