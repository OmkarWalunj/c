/* WAp  that accept number from user seperate digits from that number and enter them sort the array in assending order
 *	input : 94211423
 *	op    : 1  1  2  2  3  4  4  9
 * */

#include <stdio.h>

void main(){

	int num;
	printf("Enter number :\n");
	scanf("%d",&num);

	int arr[10];
	int x=0,rem;

	while(num !=0){
		rem=num%10;
		arr[x]=rem;
		num=num/10;
		x++;
	}

	int temp;
	for(int i=0;i<x;i++){
		for(int j=i+1;j<x;j++){
			if(arr[i]>arr[j]){
				temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
	}
	for (int i=0;i<x;i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
}
	
