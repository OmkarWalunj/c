/* Wap that accept a string from user and prints the lenth of the string use mystrlen()
 * */

#include <stdio.h>

int mystrlen(char * str){
	int count=0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}
void main(){
	int size;
	printf("Enter size of string:\n");
	scanf("%d",&size);

	char arr[size];
	//char arr[]={'a'};
	printf("Enter string:\n");
	gets(arr);
	puts(arr);
	printf("the length of entered string is %d\n",mystrlen(arr));
}
