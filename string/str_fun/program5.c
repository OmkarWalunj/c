// strcat   =  string concat


#include <stdio.h>
#include <string.h>

void main(){

	char str1[20] = "omkar";
	char *str2="walunj";

	puts(str1);   //omkar
	puts(str2);   //walunj

	strcat(str1,str2);    //strcat  (src,dest)
	puts(str1);   //omkarwalunj
	puts(str2);  //walunj
}
