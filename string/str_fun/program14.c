// mystrncat   =  string concat


#include <stdio.h>
//#include <string.h>

char * mystrncat(char *, char*,int );
void main(){

	char str1[20] = "Hardik ";
	char *str2="Pandya";

	puts(str1);
	puts(str2);

	mystrncat(str1,str2,5);    //strcpy  (dest,src)
	puts(str1);
	puts(str2);
}

char * mystrncat(char * dest, char*src,int num){
	while(*dest != '\0'){
		dest++;
	}
	int i=0;
	while( *src != '\0' && i<num){

		*dest = *src;
		i++;
		src++;
		dest++;
	}
	*dest = '\0';
	return dest;
}

