//  strrev == reverse string

#include <stdio.h>

char * strrev(char*);

void main(){

	char str1[10]="Kanha";
	
	//puts(str1);
	strrev(str1);
	puts(str1);
}

char * strrev(char * str1 ){
	char * ch1 = str1;

	while( *ch1 != '\0'){
		ch1++;
	}
	ch1--;
	char var;
	while( str1<ch1){

		var = *str1;
		*str1 = *ch1;
		*ch1 = var;

		ch1--;
		str1++;
	}
	return str1;
}

