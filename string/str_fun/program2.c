//mystrlen  =string length;

#include <stdio.h>

int mystrlen(char * );
void main(){
	char name []={'K','L','R','a','h','u','l','\0'};
	char * str ="Virat Kohli";

	int lenname= mystrlen(name);
	int lenstr=mystrlen(str);

	printf("%d\n",lenname);
	printf("%d\n",lenstr);
}

int mystrlen(char * arr){

	int count =0;

	while ( *arr != 0){

		count ++;
		arr++;
	}
	return count;
}
