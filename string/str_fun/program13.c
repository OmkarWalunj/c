// mystrncpy   =  string copy


#include <stdio.h>
//#include <string.h>

char * mystrncpy(char *,const char*,int );
void main(){

	char * str1 = "Hardik Pandya";
	char str2[20];

	puts(str1);
	puts(str2);

	mystrncpy(str2,str1,5);    //strcpy  (dest,src)
	puts(str1);
	puts(str2);
}

char * mystrncpy(char * dest, const char*src,int num){
	int i=0;
	while( *src != '\0' && i<num){

		*dest = *src;
		i++;
		src++;
		dest++;
	}
	*dest = '\0';
	return dest;
}

