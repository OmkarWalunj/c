// strcat   =  string concat


#include <stdio.h>
char * mystrcat(char*,char*);

void main(){

	char str1[20] = "omkar";
	char *str2="walunj";

	puts(str1);   //omkar
	puts(str2);   //walunj

	mystrcat(str1,str2);    //strcat  (src,dest)
	puts(str1);   //omkarwalunj
	puts(str2);  //walunj
}

char * mystrcat(char * src ,char * dest){

	while(*src !='\0'){
		src++;
	}
	while(*dest !='\0'){
		*src = *dest;
		src++;
		dest++;
	}
	*src = '\0';
	return src;
}
