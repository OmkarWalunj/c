// Array of character pointer

#include <stdio.h>
#include<string.h>
void main(){

	char *arr1[]={"Omkar", "Tejas", "Omkar"};

	printf("%p\n",arr1[0]);   //0x100
	printf("%p\n",arr1[1]);   //0x110
	printf("%p\n",arr1[2]);  //0x100

	puts(arr1[0]);      //Omkar
	printf("%s\n",arr1[2]);  //Omkar

	strcpy(arr1[2],"Rahul"); //segmentation fault
	
	puts(arr1[0]);   
	printf("%s\n",arr1[2]);   
}
