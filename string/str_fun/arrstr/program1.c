// Array of string

#include <stdio.h>
#include<string.h>
void main(){

	char arr1[][10]={"Omkar", "Tejas", "Omkar"};
	char arr2[][10]={
				{'O','m','k','a','r','\0'},
				{'T','e','j','a','s','\0'},
				{'O','m','k','a','r','\0'}};
	printf("%p\n",arr1[0]);   //0x100
	printf("%p\n",arr1[1]);   //0x110
	printf("%p\n",arr1[2]);  //0x120

	puts(arr1[0]);      //Omkar
	printf("%s\n",arr1[2]);  //Omkar

	strcpy(arr1[2],"Rahul");
	
	puts(arr1[0]);   //Omkar
	printf("%s\n",arr1[2]);   //Rahul
}
