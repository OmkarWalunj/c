//Wap to create an 1D,2D,3D array and print using pointer;

#include <stdio.h>

void main(){

	int arr1[10]={1,2,3,4,5,6,7,8,9,10};
	int arrsize1=sizeof(arr1)/sizeof(int);
	int arr2[3][3]={1,2,3,4,5,6,7,8,9};
	int arrsize2=sizeof(arr2)/sizeof(int);
	int arr3[2][3][3]={{1,2,3,4,5,6,7,8,9},{10,11,12,13,14,15,16,17,18}};
	int arrsize3=sizeof(arr3)/sizeof(int);

	printf("print 1D array:\n");
	for (int i=0;i<arrsize1;i++){
		printf("%d\n",*(arr1+i));
	}
	printf("print 2D array:\n");
	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			printf("%d  ",*(*(arr2+i)+j));
		}
		printf("\n");
	}

	printf("print 3D array:\n");
	for (int i=0;i<2;i++){
		printf("Plane %d",i);
		for (int j=0;j<3;j++){
			for (int k=0;k<3;k++){
				printf("%d  ",*(*(*(arr3+i)+j)+k));
			}
			printf("\n");
		}
	}
		
}

