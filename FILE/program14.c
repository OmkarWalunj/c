// fseek

#include <stdio.h>

void main(){

	FILE * fp=fopen("Info.txt","r");

	fseek(fp,10,0);

	char ch ;

	while((ch=fgetc(fp))!= EOF){
		printf("%c",ch);
	}
	printf("\n");

	fseek(fp,-7,2);

	char ch1;

	while((ch1=fgetc(fp)) != EOF){

		printf("%c",ch1);
	}
	printf("\n");
}

