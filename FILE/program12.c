// use ftell() and rewind()

#include <stdio.h>

void main(){


	FILE * fp =fopen ("Info.txt","w");

	printf("%ld\n",ftell(fp));   //0

	fprintf(fp,"Omkar Walunj ");
	printf("%ld\n",ftell(fp));  //13
	fprintf(fp,"Kakanewadi \n");
	printf("%ld\n",ftell(fp));  //25
	rewind(fp);
	printf("%ld\n",ftell(fp));   //0
	fprintf(fp,"Ahamednagar,om");
	printf("%ld\n",ftell(fp));   //14  ans =Ahamednagar,omakanewadi 

}



