// fputc/fgetc

#include <stdio.h>

void main(){

	FILE * fp=fopen("Info.txt","r");
	FILE * fp2=fopen("Demo.txt","w");
	char ch ;

	while((ch=fgetc(fp))!= EOF){
		printf("%c",ch);
	}
	rewind(fp);
	while((ch=fgetc(fp)) != EOF){

		fputc(ch,fp2);
	}
}

